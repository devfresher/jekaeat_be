import crypto from 'crypto';
import PaystackService from './src/services/PaystackService.js';
import { emailQueue } from './src/jobs/QueueManager.js';
import { sendEmail } from './src/mailer/EmailService.js';
import { config } from './src/utils/config.js';
import { connectDB } from './src/db/index.js';
import logger from './src/startup/logger.js';
import OrderService from './src/services/OrderService.js';
import NewOrder from './src/db/models/NewOrder.js';
import winston from 'winston';
import PayoutService from './src/services/PayoutService.js';

const generateSecretKey = () => {
	const secretKey = crypto.randomBytes(32).toString('hex');
	winston.info('Generated Secret Key:', secretKey);
};

const simulatePaystackPayment = () => {
	PaystackService.transaction
		.initialize({
			amount: 5000,
			email: 'soliuomogbolahan01@gmail.com',
			metadata: {
				orderToken:
					'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcklkIjoiNjQ3OWNkM2FhYzhmOTAyYTVhNjZlNmU4IiwidHlwZSI6Im9yZGVyIiwib3JkZXIiOnsidmVuZG9ySWQiOiI2NDlkZmRiNjQxNmUxZjc5MWY2ZjJmY2MiLCJtZWFscyI6W3sibWVhbCI6eyJfaWQiOiI2NDlkZmU2MDQxNmUxZjc5MWY2ZjMwMzEifSwicXVhbnRpdHkiOjEsInVuaXRQcmljZSI6NTB9XSwicGF5bWVudE1ldGhvZCI6Ik9ubGluZSIsImRlbGl2ZXJ5SW5mbyI6eyJhZGRyZXNzIjoiQmFicyBvaWwgc3RhdGlvbiIsImNvbnRhY3ROYW1lIjoiVXNtYW4gU29saXUiLCJjb250YWN0UGhvbmUiOiI4MTE5OTAwODU3In19LCJpYXQiOjE2OTAzMjIwNTYsImV4cCI6MTY5MjEyMjA1Nn0.E38SYac4FCmxf8Oyyfb5ar-Db6Iu1r3xOFoBUt_KBZw',
			},
		})
		.then((response) => {
			console.log(response.data);
		})
		.catch((error) => {
			console.log(error);
		});
};

const generatePaystackSignature = () => {
	// Paystack webhook payload
	const payload = {};

	const signature = crypto
		.createHmac('sha512', config.PAYSTACK_SECRET_KEY)
		.update(JSON.stringify(payload))
		.digest('hex');
	winston.info(signature);
};

const testEmailDelivery = async () => {
	try {
		const emailData = {
			templateName: 'welcome',
			recipientEmail: 'soliuomogbolahan01@gmail.com',
			templateData: { fullName: 'Usman Soliu' },
		};

		await sendEmail(emailData);
		await emailQueue.addJob(emailData);
	} catch (error) {
		throw error;
	}
};

const orderConversion = async () => {
	const oldOrders = await OrderService.getMany({}, { page: 8, limit: 100 });

	for (const oldOrder of oldOrders.items) {
		try {
			const newOrder = new NewOrder({
				_id: oldOrder._id,
				customer: oldOrder.customer.toString(),
				vendor: oldOrder.vendor.toString(),
				rider: oldOrder.rider?.toString(),
				packages: [
					{
						name: 'Self',
						meals: oldOrder.meals,
						total: oldOrder.total,
					},
				],
				total: oldOrder.total,
				status: oldOrder.status,
				token: oldOrder.token,
				payment: oldOrder.payment,
				deliveryInfo: oldOrder.deliveryInfo,
				statusUpdates: oldOrder.statusUpdates,
				createdAt: oldOrder.createdAt,
			});

			await newOrder.save();
			winston.info(`Converted order with ID: ${oldOrder._id.toString()}`);
		} catch (error) {
			winston.error(`Order of ID ${oldOrder._id.toString()} skipped`, error);
			console.log('---------------------------------------------------');
			continue;
		}
	}

	winston.info('Conversion completed successfully!');
};

const bulkPayouts = async () => {
	try {
		let payouts = await PayoutService.getMany({
			processPeriod: 'Daily',
			status: 'Pending',
			processedAt: undefined,
		});

		await PayoutService.processBulkPayouts(payouts);
	} catch (error) {
		throw error;
	}
};

logger();
connectDB()
	.then(async (connection) => {
		winston.info('DB Connected');

		// await bulkPayouts();
		// await orderConversion();
		// await testEmailDelivery();
		// generateSecretKey();
		// simulatePaystackPayment();
		// generatePaystackSignature();
	})
	.catch((error) => {
		winston.error('Error connecting to MongoDB', error);
		process.exit(0);
	});
