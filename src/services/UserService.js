import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import User from '../db/models/User.js';
import { paginationLabel } from '../utils/pagination.util.js';
import { config } from '../utils/config.js';
import FileService from './FileService.js';
import { emailQueue } from '../jobs/QueueManager.js';
import { sendEmail } from '../mailer/EmailService.js';
import { now } from '../utils/day.util.js';
import { BadRequestError, SystemError, UnauthorizedError } from '../errors/index.js';

export default class UserService {
	static model = User;
	static sensitiveData = {
		password: 0,
		resetPasswordToken: 0,
		resetTokenExpiry: 0,
	};

	static async getOne(filterQuery, sensitive = true) {
		const projection = sensitive ? this.sensitiveData : '';
		const user = await this.model.findOne(filterQuery, projection);

		return user || null;
	}

	static async count(filterQuery) {
		const count = await this.model.countDocuments(filterQuery);
		return count;
	}

	static async getMany(filterQuery, pageFilter, sensitive = true) {
		const projection = sensitive ? this.sensitiveData : '';
		let query = this.model.find(filterQuery, projection).sort({ createdAt: -1 });

		if (pageFilter && (pageFilter.page || pageFilter.limit)) {
			const options = {
				customLabels: paginationLabel,
				select: projection,
				sort: { createdAt: -1 },
			};
			query = this.model.paginate(filterQuery, { ...pageFilter, ...options });
		}

		return await query;
	}

	static async getOverallUserStatistics() {
		const currentMonth = now().startOf('month').toDate();
		const previousMonth = now().subtract(1, 'month').startOf('month').toDate();
		const nextMonth = now().add(1, 'month').startOf('month').toDate();

		// Total number of vendors
		const totalUsers = await this.count();

		// Number of vendors registered in the last month
		const lastMonthUsers = await this.count({
			createdAt: {
				$gte: previousMonth,
				$lt: currentMonth,
			},
		});

		// Number of vendors registered this month
		const currentMonthUsers = await this.count({
			createdAt: {
				$gte: currentMonth,
				$lt: nextMonth,
			},
		});

		let percentageChange = 0;
		if (lastMonthUsers !== 0) {
			percentageChange = ((currentMonthUsers - lastMonthUsers) / totalUsers) * 100;
		}

		return {
			totalUsers,
			lastMonthUsers,
			currentMonthUsers,
			percentageChange,
		};
	}

	static async changeUserPassword(userId, currentPassword, newPassword) {
		let user = await this.getOne({ _id: userId }, false);
		if (!user) throw new UnauthorizedError('Invalid user');

		const isValidCurrentPassword = await bcrypt.compare(currentPassword, user.password);
		if (!isValidCurrentPassword) throw new UnauthorizedError('Invalid current password.');

		const isSamePassword = await bcrypt.compare(newPassword, user.password);
		if (isSamePassword)
			throw new SystemError(
				422,
				'Unable to update password. Please ensure that the current password and new password are not the same.'
			);

		user = await this.resetUserPassword(user, newPassword);
		return user;
	}

	static async resetUserPassword(user, newPassword) {
		user.password = await this.hashPassword(newPassword);
		if (user.passwordReset) user.passwordReset.expiry = now().toDate();

		await user.save();

		user = await this.getOne({ _id: user._id });

		const emailData = {
			templateName: 'password_reset_successful',
			recipientEmail: user.email,
			templateData: { user },
		};
		emailQueue.addJob(emailData);
		sendEmail(emailData);
		return user;
	}

	static async updateProfile(user, updateData) {
		if (Object.keys(updateData).length === 0) {
			throw new BadRequestError('Nothing to update');
		}

		const {
			fullName,
			phoneNumber,
			description,
			deliveryAddress,
			schedule,
			passwordReset,
			restaurantName,
			restaurantLocation,
			riderType,
			partner,
		} = updateData;

		user.fullName = fullName ?? user.fullName;
		user.phoneNumber = phoneNumber ?? user.phoneNumber;
		user.passwordReset = passwordReset ?? user.passwordReset;

		switch (user.__t) {
			case 'Vendor':
				user.restaurantName = restaurantName ?? user.restaurantName;
				user.restaurantLocation = restaurantLocation ?? user.restaurantLocation;
				user.schedule = schedule ?? user.schedule;
				user.description = description ?? user.description;
				break;
			case 'Customer':
				user.deliveryAddress = deliveryAddress ?? user.deliveryAddress;
				break;
			case 'Rider':
				user.schedule = schedule ?? user.schedule;
				user.type = riderType ?? user.type;
				user.partner = {
					companyName: partner.companyName ?? user.partner.companyName,
					ride: {
						plateNumber: partner.ride.plateNumber ?? user.partner.ride.plateNumber,
						description: partner.ride.description ?? user.partner.ride.description,
						type: partner.ride.type ?? user.partner.ride.type,
					},
				};
				break;
			default:
				throw new BadRequestError('Invalid user type');
		}

		await user.save();
		return user;
	}

	static async updateSettlementAccount(user, updateData) {
		const { accountNumber, accountName, bankCode, allowPeriodicPayout, period } = updateData;

		let account;
		const { payoutSettlement } = user;
		if (payoutSettlement) account = payoutSettlement.account;

		const newAccount = {
			accountName: accountName ?? account?.accountName,
			accountNumber: accountNumber ?? account?.accountNumber,
			bank: bankCode ?? account?.bank,
		};

		user.payoutSettlement = {
			account: newAccount ?? account,
			allowPeriodicPayout: allowPeriodicPayout ?? payoutSettlement?.allowPeriodicPayout,
			period: period ?? payoutSettlement?.period,
		};

		await user.save();
		return this.getOne({ _id: user.id });
	}

	static async updateProfileImage(user, imageData) {
		const imageId = user.profileImage?.imageId;
		const { profileImage, file } = imageData;

		let newImage;
		if (profileImage) {
			newImage = {
				secure_url: profileImage,
			};
		} else if (file) {
			newImage = await FileService.uploadToCloudinary(file, 'users', imageId);
		}

		if (newImage) {
			user.profileImage = { url: newImage.secure_url, imageId: newImage.public_id };
			await user.save();
		}

		return user;
	}

	static async toggleUserActivation(user, status) {
		if (status && status == user.isActive)
			throw new BadRequestError(`Account already ${!status ? 'de-' : ''}activated`);

		user.isActive = status || !user.isActive;
		await user.save();
		return user;
	}

	static async delete(user) {
		if (user.profileImage?.imageId)
			await FileService.deleteFromCloudinary(user.profileImage.imageId);

		const deleted = await this.model.findOneAndDelete({ _id: user._id });
		return deleted;
	}

	static async generateAuthToken(user, admin = false) {
		const expiry = 5 * 60 * 1000; // 5 minutes of token expiry
		try {
			const token = jwt.sign(
				{
					_id: user._id,
					userType: admin ? 'Admin' : user.__t,
				},
				config.JWT_PRIVATE_KEY,
				{ expiresIn: expiry }
			);
			return { token, expiresAt: now().add(expiry, 'millisecond').toDate() };
		} catch (error) {
			throw new SystemError(500, 'Failed to generate auth token.', error);
		}
	}

	static async hashPassword(password) {
		try {
			const salt = await bcrypt.genSalt(10);
			const hashedPassword = await bcrypt.hash(password, salt);
			return hashedPassword;
		} catch (error) {
			throw new SystemError(500, 'Failed to hash password.', error);
		}
	}

	static async isPayoutAccountComplete(user) {
		const { account } = user.payoutSettlement;

		if (!account || !account.accountName || !account.accountNumber || !account.bank) {
			return false;
		}

		return true;
	}
}
