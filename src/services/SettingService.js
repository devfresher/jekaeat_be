import Setting from '../db/models/Settings.js';
import { now } from '../utils/day.util.js';

class SettingService {
	// Retrieve a single setting by its unique key
	getOne = async (key) => {
		const setting = await Setting.findOne({ key });
		return setting?.value;
	};

	// Retrieve multiple settings based on a filter query
	getMany = async (filterQuery) => {
		const settings = await Setting.find(filterQuery);
		return settings;
	};

	// Update/Insert the value of a setting identified by its key
	upsert = async (key, value) => {
		const setting = await Setting.findOneAndUpdate(
			{ key },
			{ value, updatedAt: now().toDate() },
			{ new: true, upsert: true }
		);
		return setting;
	};
}

export default new SettingService();
