import crypto from 'crypto';
import bcrypt from 'bcryptjs';
import UserService from './UserService.js';
import AdminService from './AdminService.js';
import { emailQueue } from '../jobs/QueueManager.js';
import { sendEmail } from '../mailer/EmailService.js';
import dayjs, { now } from '../utils/day.util.js';
import { BadRequestError, ConflictError, ForbiddenError } from '../errors/index.js';

export default class AuthService {
	static async signIn(email, password, userType) {
		const user = await UserService.getOne({ email }, false);

		if (!user) throw new BadRequestError('Invalid email address or password');

		if (!user.isActive)
			throw new ForbiddenError(
				'Your account has been deactivated. Please contact support for assistance.'
			);

		if (userType !== user.__t)
			throw new ForbiddenError(
				`Access Denied: ${user.__t}s are not allowed to access ${userType}'s app`
			);

		const isValidPassword = await bcrypt.compare(password, user.password);
		if (!isValidPassword) throw new BadRequestError('Invalid email address or password');

		delete user.password;
		const accessToken = await UserService.generateAuthToken(user);

		return { user, accessToken };
	}

	static async sendOtp(email) {
		const user = await UserService.getOne({ email });
		if (!user) throw new BadRequestError('User with this email does not exist');

		const expiresIn = 10; // In minutes
		const { otpCode, expiry } = this.generateCode(expiresIn);

		const timeUsedAfterGen = expiresIn - dayjs(user.passwordReset?.expiry).diff(now, 'minute');
		if (user.passwordReset && timeUsedAfterGen < 2) {
			throw new ConflictError(`Try again after ${2 - timeUsedAfterGen} minutes`);
		}

		await UserService.updateProfile(user, {
			passwordReset: { token: otpCode, expiry },
		});

		const emailData = {
			templateName: 'reset_password',
			recipientEmail: user.email,
			templateData: {
				user,
				token: {
					otpCode,
					expiry: `${expiresIn} minutes`,
				},
			},
		};
		emailQueue.addJob(emailData);
		sendEmail(emailData);
	}

	static async validateOtp(user, otpCode) {
		const { passwordReset } = user;

		if (otpCode !== passwordReset.token) throw new BadRequestError('Invalid otp code');
		if (passwordReset.expiry < now().toDate()) throw new BadRequestError('Otp code expired');

		return;
	}

	static async adminSignIn(email, password) {
		let admin = await AdminService.getOne({ email }, false);
		if (!admin) throw new BadRequestError('Invalid email address or password');

		const isValidPassword = await bcrypt.compare(password, admin.password);
		if (!isValidPassword) throw new BadRequestError('Invalid email address or password');

		const accessToken = await UserService.generateAuthToken(admin, true);
		admin = await AdminService.getOne({ email });

		return { admin, accessToken };
	}

	static generateCode(expiresIn = 10, length = 6) {
		expiresIn = expiresIn * 60 * 1000;
		const allowedChars = '0123456789';

		let otpCode = '';
		while (otpCode.length < length) {
			const charIndex = crypto.randomInt(0, allowedChars.length);

			if (otpCode.length === 0 && allowedChars[charIndex] === '0') {
				continue;
			}
			otpCode += allowedChars[charIndex];
		}

		const expiry = now().add(expiresIn, 'millisecond').toDate();

		return { otpCode, expiry };
	}
}
