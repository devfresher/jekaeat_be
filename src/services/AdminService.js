import bcrypt from 'bcryptjs';
import Admin from '../db/models/Admin.js';
import { paginationLabel } from '../utils/pagination.util.js';
import { BadRequestError, ForbiddenError, NotFoundError } from '../errors/index.js';

const sensitiveData = '-password -resetPasswordToken -resetTokenExpiry';

export default class AdminService {
	static model = Admin;

	static async getOne(filterQuery, sensitive = true) {
		const projection = sensitive ? sensitiveData : '';
		const admin = await this.model.findOne(filterQuery, projection);

		return admin || null;
	}

	static async getMany(filterQuery, pageFilter, sensitive = true) {
		const projection = sensitive ? sensitiveData : '';
		let query = this.model.find(filterQuery, projection).sort({ name: 1 });

		if (pageFilter && (pageFilter.page || pageFilter.limit)) {
			const options = {
				customLabels: paginationLabel,
				select: projection,
				sort: { name: 1 },
			};
			query = this.model.paginate(filterQuery, { ...pageFilter, ...options });
		}

		return await query;
	}

	static async updateAdminPassword(adminId, currentPassword, newPassword) {
		const admin = await this.getOne({ _id: adminId });
		if (!admin) throw new ForbiddenError('Invalid admin');

		const isValidCurrentPassword = await bcrypt.compare(currentPassword, admin.password);
		if (!isValidCurrentPassword)
			throw new BadRequestError(
				'Invalid current password. Please enter the correct current password and try again.'
			);

		const isSamePassword = await bcrypt.compare(newPassword, admin.password);
		if (isSamePassword)
			throw new BadRequestError(
				'Unable to update password. Please ensure that the current password and new password are not the same.'
			);

		admin.password = await this.hashPassword(newPassword);
		await admin.save();
		return admin;
	}

	static async updateProfile(admin, updateData) {
		if (Object.keys(updateData).length === 0) {
			throw new BadRequestError('Nothing to update');
		}

		const { fullName, phoneNumber } = updateData;

		admin.fullName = fullName ?? admin.fullName;
		admin.phoneNumber = phoneNumber ?? admin.phoneNumber;

		await admin.save();
		return admin;
	}

	static async deactivateAdmin(adminId) {
		const admin = await this.getOne({ _id: adminId });
		if (!admin) throw new NotFoundError('admin not found');

		admin.isActive = false;
		await admin.save();
		return admin;
	}
}
