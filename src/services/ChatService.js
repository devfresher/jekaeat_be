import { now } from '../utils/day.util.js';
import ChatMessage from '../db/models/ChatMessage.js';

const chatPopulateData = [
	{ path: 'sender', select: '_id fullName restaurantName profileImage' },
	{ path: 'recipient', select: '_id fullName restaurantName profileImage' },
];
export default class ChatService {
	static async getOne(filterQuery) {
		const message = await ChatMessage.findOne(filterQuery).populate(chatPopulateData);

		if (!message) return false;
		return message;
	}

	static async sendMessage(senderId, recipientId, message, messageType) {
		const chatMessage = new ChatMessage({
			sender: senderId,
			recipient: recipientId,
			message,
			messageType,
			createdAt: now().toDate(),
		});

		await chatMessage.save();
		return await this.getOne({ _id: chatMessage._id });
	}

	static async markAsRead(messageId) {
		const message = await ChatMessage.findByIdAndUpdate(
			messageId,
			{ readAt: now().toDate() },
			{ new: true }
		);

		if (!message) return false;
		return message;
	}

	static async getAllChatsByUser(userId) {
		const chats = await ChatMessage.find({ $or: [{ sender: userId }, { recipient: userId }] })
			.sort({ createdAt: -1 })
			.populate(chatPopulateData);

		const chatsByUser = chats.reduce((acc, chat) => {
			const otherUser =
				chat.sender._id.toString() === userId.toString() ? chat.recipient : chat.sender;
			const otherUserChats = acc.find((userChat) => userChat.user === otherUser);

			if (otherUserChats) {
				otherUserChats.chats.push(chat);
			} else {
				acc.push({ user: otherUser, chats: [chat] });
			}
			return acc;
		}, []);

		return chatsByUser;
	}

	static async groupUserChats(userId) {
		const chats = await ChatMessage.find({ $or: [{ sender: userId }, { recipient: userId }] })
			.sort({ createdAt: -1 })
			.populate(chatPopulateData);

		const chatsByUser = chats.reduce((acc, chat) => {
			const otherUser =
				chat.sender && chat.sender?._id.toString() === userId.toString()
					? chat.recipient
					: chat.sender;

			const otherUserChats = acc.find((userChat) => userChat === otherUser);

			if (!otherUserChats) {
				acc.push(otherUser);
			}

			return acc;
		}, []);

		return chatsByUser;
	}

	static async getConversations(user1, user2) {
		const chats = await ChatMessage.find({
			$or: [
				{ sender: user1, recipient: user2 },
				{ sender: user2, recipient: user1 },
			],
		})
			.sort({ createdAt: -1 })
			.populate(chatPopulateData);

		return chats;
	}
}
