import { Paystack } from 'paystack-sdk';
import { config } from '../utils/config.js';
import OrderService from './OrderService.js';
import VendorService from './VendorService.js';
import PayoutService from './PayoutService.js';
import { BadRequestError, NotFoundError } from '../errors/index.js';

class PaystackService extends Paystack {
	#handlers;
	#paystack;

	constructor(secretKey) {
		super(secretKey);
		this.#paystack = new Paystack(secretKey);
		this.#handlers = {
			'charge.success': this.handleChargeSuccess.bind(this),
			'charge.failed': this.handleChargeFailed.bind(this),
			'transfer.success': this.handleTransferSuccess.bind(this),
			'transfer.failed': this.handleTransferFailed.bind(this),
			'transfer.reversed': this.handleTransferReversed.bind(this),
		};
	}

	getBalance = async () => {
		const balance = await this.#paystack.transfer.control.balance();
		return balance;
	};

	verifyAccountNumber = async (accountNumber, bankCode) => {
		const bankAccount = await this.#paystack.verification.resolveAccount({
			account_number: accountNumber,
			bank_code: bankCode,
		});

		if (!bankAccount.status) throw new BadRequestError(bankAccount.message);
		return bankAccount.data;
	};

	handleWebhook = async (event, data) => {
		try {
			const handler = this.#handlers[event];
			if (!handler) throw new BadRequestError('Unidentified event');

			return await handler(data);
		} catch (error) {
			throw error;
		}
	};

	handleChargeSuccess = async (data) => {
		try {
			const {
				metadata: { orderToken },
				reference,
			} = data;

			let order;
			if (orderToken) {
				const decodedToken = await OrderService.decodeOrderToken(orderToken);
				const { orderInfo, customerId } = decodedToken;

				order = await OrderService.create(customerId, reference, orderInfo, orderToken);
				await PayoutService.checkPayout(order, 'vendor');
				await OrderService.updatePaymentStatus(order, 'Paid');

				const vendor = await VendorService.getOne({ _id: order.vendor._id });
				const vendorPayoutPeriod = vendor?.payoutSettlement?.period ?? 'Immediate';

				await PayoutService.sharePayout(order, vendor, vendorPayoutPeriod);
				return order;
			} else {
				order = await OrderService.getOne({ 'payment.reference': data.reference });
				if (!order) throw new NotFoundError('Order not found');
			}
		} catch (error) {
			throw error;
		}
	};

	handleChargeFailed = async (data) => {
		try {
			const order = await OrderService.getOne({ 'payment.reference': data.reference });
			if (!order) throw new NotFoundError('Order not found');

			return await OrderService.updatePaymentStatus(order, 'Failed');
		} catch (error) {
			throw error;
		}
	};

	handleTransferSuccess = async (data) => {
		try {
			const payouts = await PayoutService.getMany({ reference: data.reference });
			if (!payouts) throw new NotFoundError('No payout found');

			payouts.forEach(async (payout) => {
				await PayoutService.updateStatus(payout, 'Successful');
			});
		} catch (error) {
			throw error;
		}
	};

	handleTransferFailed = async (data) => {
		try {
			const payouts = await PayoutService.getMany({ reference: data.reference });
			if (!payouts) throw new NotFoundError('No payout found');

			payouts.forEach(async (payout) => {
				await PayoutService.updateStatus(payout, 'Failed');
			});
		} catch (error) {
			throw error;
		}
	};

	handleTransferReversed = async (data) => {
		try {
			const payouts = await PayoutService.getMany({ reference: data.reference });
			if (!payouts) throw new NotFoundError('No payout found');

			payouts.forEach(async (payout) => {
				await PayoutService.updateStatus(payout, 'Reversed');
			});
		} catch (error) {
			throw error;
		}
	};
}

export default new PaystackService(config.PAYSTACK_SECRET_KEY);
