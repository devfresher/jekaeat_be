import Utils from '../utils/Utils.js';
import UserService from './UserService.js';
import OrderService from './OrderService.js';
import Payout from '../db/models/Payout.js';
import PaystackService from './PaystackService.js';
import { processPayoutQueue } from '../jobs/QueueManager.js';
import { paginationLabel } from '../utils/pagination.util.js';
import { now } from '../utils/day.util.js';
import { BadRequestError, ForbiddenError, SystemError } from '../errors/index.js';

const payoutPopulateData = [
	{ path: 'user', select: '_id fullName profileImage' },
	{ path: 'order', populate: { path: 'packages.meals.meal' } },
];

export default class PayoutService {
	static async sharePayout(order, receiver, processPeriod = 'Immediate') {
		const payout = await this.create(receiver._id, order._id, processPeriod);

		// Process transfer if period is immediate
		if (processPeriod === 'Immediate') {
			await processPayoutQueue.addJob(payout);
			try {
				await PayoutService.processPayout(payout);
			} catch (error) {
				throw error;
			}
		}
	}

	static async create(userId, orderId, processPeriod) {
		const user = await UserService.getOne({ _id: userId });
		if (!user) throw new BadRequestError('Invalid user');

		const order = await OrderService.getOne({ _id: orderId });
		if (!order) throw new BadRequestError('Invalid order');

		const amount = user.__t === 'Vendor' ? order.total : order.deliveryInfo?.ridersFee;
		const newPayout = Payout({
			user: userId,
			order: orderId,
			amount,
			status: 'Pending',
			processPeriod,
		});

		await newPayout.save();

		const payout = await this.getOne({ _id: newPayout._id });
		return payout;
	}

	static async getOne(filterQuery) {
		const payout = await Payout.findOne(filterQuery).populate(payoutPopulateData);
		return payout || null;
	}

	static async getMany(filterQuery, pageFilter) {
		const options = {
			sort: { createdAt: -1 },
			populate: payoutPopulateData,
		};

		if (!pageFilter) return await Payout.find(filterQuery, null, options);

		const paging = {
			...options,
			customLabels: paginationLabel,
		};

		if (pageFilter.page) {
			paging.page = pageFilter.page;
		}

		if (pageFilter.limit) {
			paging.limit = pageFilter.limit;
		}

		return await Payout.paginate(filterQuery, paging);
	}

	static async update(payout, updateData) {
		if (Object.keys(updateData).length === 0) throw new BadRequestError('Nothing to update');

		const { processPeriod, processedAt } = updateData;

		payout.processPeriod = processPeriod ?? payout.processPeriod;
		payout.processedAt = processedAt ?? payout.processedAt;

		await payout.save();
		return await this.getOne({ _id: payout._id });
	}

	static async updateStatus(payout, newStatus) {
		if (payout.status === newStatus) return;

		if (payout.status === 'Successful') {
			const verified = await PaystackService.transfer.verify(payout.reference);
			if (!verified) {
				throw new SystemError(402, 'Transfer not successful');
			}
		}

		payout.status = newStatus;
		await payout.save();

		return await this.getOne({ _id: payout._id });
	}

	static async checkPayout(order, payoutType) {
		const payout = await this.getOne({
			order: order._id,
			user: payoutType == 'vendor' ? order.vendor._id : order.rider._id,
			$or: [{ status: 'Successful' }, { status: 'Pending' }],
		});

		if (payoutType == 'vendor') {
			if (order.payment.status === 'Paid' && payout) {
				if (payout.status === 'Successful') {
					throw new ForbiddenError('Payout already initiated and processed');
				} else {
					throw new ForbiddenError('Payout already initiated');
				}
			}
		} else if (payoutType == 'rider') {
			if (payout) {
				if (payout.status === 'Successful') {
					throw new ForbiddenError('Payout already initiated and processed');
				} else {
					throw new ForbiddenError('Payout already initiated');
				}
			}
		}
	}

	// static async #createTransferData({ _id: orderId, packages }, amount, recipientId, reference) {
	// 	const transferData = {
	// 		source: 'balance',
	// 		amount: amount * 100,
	// 		recipient: recipientId,
	// 		reason: `Payout for ${packages[0].meals[0].meal.name} (#${orderId})`,
	// 		reference,
	// 	};
	// 	return transferData;
	// }

	static async #createTransferData(payouts, recipientId, reference) {
		// Calculate the total amount for all payouts
		const totalAmount = payouts.reduce((acc, payout) => acc + payout.amount * 100, 0);

		// Create a comma-separated list of package names and order IDs
		const payoutDescriptions = payouts
			.map((payout) => {
				const {
					order: { _id: orderId, packages },
				} = payout;
				
				const firstMealName = packages[0].meals[0].meal.name;
				return `${firstMealName} (#${orderId})`;
			})
			.join(', ');

		const transferData = {
			source: 'balance',
			amount: totalAmount,
			recipient: recipientId,
			reason: `Payout for ${payoutDescriptions}`,
			reference,
		};

		return transferData;
	}

	static async processPayout(payout) {
		let { user, order, reference } = payout;

		if (payout.processedAt) throw new ForbiddenError('Payout already processed');

		user = await UserService.getOne({ _id: user._id });
		order = await OrderService.getOne({ _id: order._id });

		const recipientData = await this.#createRecipientData(user);
		const recipient = await PaystackService.recipient.create(recipientData);
		const recipientId = recipient?.data?.recipient_code;

		if (!recipientId)
			throw new SystemError(
				400,
				`Error creating transfer recipient ${recipient.message}`,
				recipient
			);

		if (!reference) {
			reference = Utils.generateReference();
			payout.reference = reference;
			await payout.save();
		}

		const transferData = await this.#createTransferData([payout], recipientId, reference);
		const transfer = await PaystackService.transfer.initiate(transferData);
		if (!transfer.status)
			throw new SystemError(
				400,
				`Payment partner could not process the request: ${transfer.message}`,
				transfer
			);

		payout.processedAt = now().toDate();
		await payout.save();
	}

	static async processBulkPayouts(payouts) {
		// Create a map to accumulate payouts for each user
		const userPayoutsGroup = new Map();
		const unprocessedPayouts = payouts.filter((payout) => !payout.processedAt);

		// Group payouts by user
		unprocessedPayouts.forEach((payout) => {
			if (userPayoutsGroup.has(payout.user._id)) {
				userPayoutsGroup.get(payout.user._id).push(payout);
			} else {
				userPayoutsGroup.set(payout.user._id, [payout]);
			}
		});

		for (const [userId, userPayouts] of userPayoutsGroup) {
			const user = await UserService.getOne({ _id: userId });

			const recipientData = await this.#createRecipientData(user);
			const recipient = await PaystackService.recipient.create(recipientData);
			const recipientId = recipient?.data?.recipient_code;

			if (!recipientId)
				throw new SystemError(500, 'Error creating transfer recipient', recipient);

			const reference = Utils.generateReference();
			const transferData = await this.#createTransferData(
				userPayouts,
				recipientId,
				reference
			);
			const transfer = await PaystackService.transfer.initiate(transferData);

			if (!transfer.status) {
				throw new SystemError(
					400,
					`Payment partner could not process the request: ${transfer.message}`,
					transfer
				);
			}

			// Update the processedAt field for each payout
			for (const payout of userPayouts) {
				payout.processedAt = now().toDate();
				payout.reference = reference;

				await payout.save();
			}
		}
	}

	static async #createRecipientData(receiver) {
		if (!receiver.payoutSettlement || !(await UserService.isPayoutAccountComplete(receiver)))
			throw new BadRequestError('Payout settlement account not found');

		const { account } = receiver.payoutSettlement;
		const recipientData = {
			type: 'nuban',
			name: account?.accountName,
			account_number: account?.accountNumber,
			bank_code: account.bank,
			currency: 'NGN',
			description: `Transfer recipient for ${receiver.fullName}`,
			metadata: {
				receiverId: receiver._id,
			},
		};
		return recipientData;
	}
}
