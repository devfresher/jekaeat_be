import Vendor from '../db/models/Vendor.js';
import { paginationLabel } from '../utils/pagination.util.js';
import UserService from './UserService.js';
import OrderService from './OrderService.js';
import Utils from '../utils/Utils.js';
import { ConflictError, NotFoundError } from '../errors/index.js';

export default class VendorService extends UserService {
	static model = Vendor;

	static async create(userData) {
		const user = await UserService.getOne({ email: userData.email });
		if (user) throw new ConflictError('Email address already taken.');

		let newVendor = new Vendor({
			fullName: userData.fullName,
			email: userData.email,
			password: await this.hashPassword(userData.password),
			phoneNumber: userData.phoneNumber,
			restaurantName: userData.restaurantName,
		});

		await newVendor.save();
		const accessToken = await UserService.generateAuthToken(newVendor);

		return { newVendor, accessToken };
	}

	static async toggleVendorFeatured(vendorId) {
		const vendor = await VendorService.getOne({ _id: vendorId });
		if (!vendor) throw new NotFoundError('Vendor not found');

		vendor.featured = !vendor.featured;
		await vendor.save();
		return vendor;
	}

	static async toggleShopClosed(vendorId) {
		const vendor = await VendorService.getOne({ _id: vendorId });
		if (!vendor) throw new NotFoundError('Vendor not found');

		vendor.shopClosed = !vendor.shopClosed;
		await vendor.save();
		return vendor;
	}

	static async getFeaturedVendors(pageFilter) {
		const filterQuery = { featured: true };
		let query = this.model.find(filterQuery, this.sensitiveData);

		if (pageFilter && (pageFilter.page || pageFilter.limit)) {
			const options = {
				customLabels: paginationLabel,
				select: this.sensitiveData,
				sort: { createdAt: -1 },
			};
			query = this.model.paginate(filterQuery, { ...pageFilter, ...options });
		}

		const featuredVendors = await query;

		// Randomly shuffle the featured vendors
		return Array.isArray(featuredVendors)
			? Utils.shuffleArray(featuredVendors)
			: { ...featuredVendors, docs: Utils.shuffleArray(featuredVendors.items) };
	}

	static async getManyVendorsWithStats(
		filterQuery,
		pageFilter,
		sensitive = true,
		sortBy = 'restaurantName',
		sortOrder = 'asc'
	) {
		const projection = sensitive ? this.sensitiveData : '';

		let query = this.model.aggregate([
			{
				$match: filterQuery,
			},
			{
				$lookup: {
					from: 'orders',
					localField: '_id',
					foreignField: 'vendor',
					as: 'orders',
				},
			},
			{
				$addFields: {
					totalOrders: { $size: '$orders' },
					completedOrders: {
						$size: {
							$filter: {
								input: '$orders',
								as: 'order',
								cond: {
									$and: [
										{ $eq: ['$$order.status', 'Delivered'] },
										{ $eq: ['$$order.payment.status', 'Paid'] },
									],
								},
							},
						},
					},
					revenue: {
						$sum: {
							$map: {
								input: '$orders',
								as: 'order',
								in: {
									$cond: [
										{
											$and: [
												{ $eq: ['$$order.status', 'Delivered'] },
												{ $eq: ['$$order.payment.status', 'Paid'] },
											],
										},
										'$$order.total',
										0,
									],
								},
							},
						},
					},
				},
			},
			{
				$project: {
					orders: 0, // Exclude the 'orders' field from the final result
					...projection,
				},
			},
			{
				$sort: { [sortBy]: sortOrder === 'asc' ? 1 : -1 },
			},
		]);

		if (pageFilter && (pageFilter.page || pageFilter.limit)) {
			const options = {
				customLabels: paginationLabel,
			};
			query = this.model.aggregatePaginate(query, { ...pageFilter, ...options });
		} else {
			query = query.exec();
		}

		return await query;
	}

	static async getOneVendorWithStats(filterQuery) {
		const vendor = await this.getOne(filterQuery);
		const statistics = await this.calculateVendorStatistics(vendor);

		return {
			vendor,
			statistics,
		};
	}

	static async calculateVendorStatistics(vendor) {
		const totalOrders = await OrderService.count({
			vendor: vendor._id,
		});

		const completedOrders = await OrderService.count({
			vendor: vendor._id,
			'payment.status': 'Paid',
			status: 'Delivered',
		});

		const totalRevenue = await OrderService.getTotalAmountEarned(vendor, {
			'payment.status': 'Paid',
			status: 'Delivered',
		});

		return { totalOrders, completedOrders, totalRevenue };
	}
}
