import { now } from '../utils/day.util.js';
import Rider from '../db/models/Rider.js';
import UserService from './UserService.js';
import { paginationLabel } from '../utils/pagination.util.js';
import OrderService from './OrderService.js';
import { ConflictError } from '../errors/index.js';

export default class RiderService extends UserService {
	static model = Rider;

	static async create(userData) {
		const user = await UserService.getOne({ email: userData.email });
		if (user) throw new ConflictError('Email address already taken.');

		let newRider = new Rider({
			fullName: userData.fullName,
			email: userData.email,
			password: await this.hashPassword(userData.password),
			phoneNumber: userData.phoneNumber,
			type: userData.riderType,
			partner: {
				companyName: userData.partner?.companyName,
				ride: {
					plateNumber: userData.partner?.ride.plateNumber,
					description: userData.partner?.ride.description,
					type: userData.partner?.ride.type,
				},
			},
		});

		await newRider.save();
		const accessToken = await UserService.generateAuthToken(newRider);

		return { newRider, accessToken };
	}

	static async getAvailableRiders(filterQuery, pageFilter) {
		const currentDay = now().format('dddd');
		const currentTime = now().format('HH:mm');

		const query = {
			...filterQuery,
			'payoutSettlement.account': { $exists: true },
			'schedule.day': currentDay,
			$or: [
				{
					'schedule.openAt': { $lte: currentTime },
					'schedule.closeAt': { $gt: currentTime },
				},
				{ 'schedule.openAt': { $lte: currentTime }, 'schedule.closeAt': '00:00' },
				{ 'schedule.openAt': '00:00', 'schedule.closeAt': { $gt: currentTime } },
			],
		};

		const availableRiders = await this.getMany(query, pageFilter);
		return availableRiders;
	}

	static async getManyRidersWithStats(
		filterQuery,
		pageFilter,
		sensitive = true,
		sortBy = 'fullName',
		sortOrder = 'asc'
	) {
		const projection = sensitive ? this.sensitiveData : '';

		let query = this.model.aggregate([
			{
				$match: filterQuery,
			},
			{
				$lookup: {
					from: 'orders',
					localField: '_id',
					foreignField: 'rider',
					as: 'orders',
				},
			},
			{
				$addFields: {
					completedOrders: {
						$size: {
							$filter: {
								input: '$orders',
								as: 'order',
								cond: {
									$and: [{ $eq: ['$$order.status', 'Delivered'] }],
								},
							},
						},
					},
					cancelledOrders: {
						$size: {
							$filter: {
								input: '$orders',
								as: 'order',
								cond: {
									$and: [{ $eq: ['$$order.status', 'Cancelled'] }],
								},
							},
						},
					},
					revenue: {
						$sum: {
							$map: {
								input: '$orders',
								as: 'order',
								in: {
									$cond: [
										{
											$and: [
												{ $eq: ['$$order.status', 'Delivered'] },
												{ $eq: ['$$order.payment.status', 'Paid'] },
											],
										},
										'$$order.deliveryInfo.ridersFee',
										0,
									],
								},
							},
						},
					},
				},
			},
			{
				$project: {
					orders: 0, // Exclude the 'orders' field from the final result
					...projection,
				},
			},
			{
				$sort: { [sortBy]: sortOrder === 'asc' ? 1 : -1 },
			},
		]);

		if (pageFilter && (pageFilter.page || pageFilter.limit)) {
			const options = {
				customLabels: paginationLabel,
			};
			query = this.model.aggregatePaginate(query, { ...pageFilter, ...options });
		} else {
			query = query.exec();
		}

		return await query;
	}

	static async getOneRiderWithStats(filterQuery) {
		const rider = await this.getOne(filterQuery);
		const statistics = await this.calculateRiderStatistics(rider);

		return {
			rider,
			statistics,
		};
	}

	static async calculateRiderStatistics(rider) {
		const completedOrders = await OrderService.count({
			rider: rider._id,
			'payment.status': 'Paid',
			status: 'Delivered',
		});

		const revenue = await OrderService.getTotalAmountEarned(rider, {
			'payment.status': 'Paid',
			status: 'Delivered',
		});

		return { completedOrders, revenue };
	}
}
