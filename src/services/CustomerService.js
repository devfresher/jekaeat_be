import { ConflictError } from '../errors/index.js';
import Customer from '../db/models/Customer.js';
import { paginationLabel } from '../utils/pagination.util.js';
import OrderService from './OrderService.js';
import UserService from './UserService.js';

export default class CustomerService extends UserService {
	static model = Customer;

	static async create(userData) {
		const user = await UserService.getOne({ email: userData.email });
		if (user) throw new ConflictError('Email address already taken');

		let newCustomer = new Customer({
			fullName: userData.fullName,
			email: userData.email,
			password: await this.hashPassword(userData.password),
			phoneNumber: userData.phoneNumber,
			deliveryAddress: {
				...userData.deliveryAddress,
			},
		});

		await newCustomer.save();
		const accessToken = await UserService.generateAuthToken(newCustomer);

		return { newCustomer, accessToken };
	}

	static async getManyCustomersWithStats(
		filterQuery,
		pageFilter,
		sensitive = true,
		sortBy = 'fullName',
		sortOrder = 'asc'
	) {
		const projection = sensitive ? this.sensitiveData : '';

		let query = this.model.aggregate([
			{
				$match: filterQuery,
			},
			{
				$lookup: {
					from: 'orders',
					localField: '_id',
					foreignField: 'customer',
					as: 'orders',
				},
			},
			{
				$addFields: {
					totalOrders: { $size: '$orders' },
				},
			},
			{
				$project: {
					orders: 0, // Exclude the 'orders' field from the final result
					...projection,
				},
			},
			{
				$sort: { [sortBy]: sortOrder === 'asc' ? 1 : -1 },
			},
		]);

		if (pageFilter && (pageFilter.page || pageFilter.limit)) {
			const options = {
				customLabels: paginationLabel,
			};
			query = this.model.aggregatePaginate(query, { ...pageFilter, ...options });
		} else {
			query = query.exec();
		}

		return await query;
	}

	static async getOneCustomerWithStats(filterQuery) {
		const customer = await this.getOne(filterQuery);
		const statistics = await this.calculateCustomerStatistics(customer);

		return {
			customer,
			statistics,
		};
	}

	static async calculateCustomerStatistics(customer) {
		const totalOrders = await OrderService.count({
			customer: customer._id,
		});

		return { totalOrders };
	}
}
