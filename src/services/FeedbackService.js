import { now } from '../utils/day.util.js';
import Feedback from '../db/models/Feedback.js';
const feedbackPopulateData = [{ path: 'user', select: '_id fullName profileImage __t' }];

export default class FeedbackService {
	static async getOne(filterQuery) {
		const feedback = await Feedback.findOne(filterQuery).populate(feedbackPopulateData);
		return feedback || null;
	}

	static async getMany(filterQuery, pageFilter) {
		if (!pageFilter || (!pageFilter.page && !pageFilter.limit))
			return await Feedback.find(filterQuery)
				.sort({ createdAt: 1 })
				.populate(feedbackPopulateData);

		pageFilter.customLabels = paginationLabel;
		pageFilter.populate = feedbackPopulateData;
		pageFilter.sort = { createdAt: 1 };

		return await Feedback.paginate(filterQuery, pageFilter);
	}

	static async sendFeedback(userId, message) {
		const feedback = new Feedback({
			user: userId,
			message,
		});

		await feedback.save();
		return await this.getOne({ _id: feedback._id });
	}

	static async markAsRead(feedbackId) {
		const message = await Feedback.findByIdAndUpdate(feedbackId, { readAt: now().toDate() });

		if (!message) return false;
		return message;
	}
}
