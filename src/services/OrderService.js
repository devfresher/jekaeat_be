import jwt from 'jsonwebtoken';
import mongoose, { Types } from 'mongoose';

import Order from '../db/models/Order.js';

import CustomerService from './CustomerService.js';
import VendorService from './VendorService.js';
import MealService from './MealService.js';
import RiderService from './RiderService.js';
import Notification from '../notification/NotificationService.js';

import { pushNotificationQueue } from '../jobs/QueueManager.js';
import PaystackService from './PaystackService.js';

import Utils from '../utils/Utils.js';
import { config } from '../utils/config.js';
import { paginationLabel } from '../utils/pagination.util.js';
import { sendEmail } from '../mailer/EmailService.js';
import { now } from '../utils/day.util.js';
import {
	BadRequestError,
	ConflictError,
	ForbiddenError,
	NotFoundError,
	SystemError,
	UnauthorizedError,
} from '../errors/index.js';
import PayoutService from './PayoutService.js';
import redisClient from '../utils/redis.util.js';

const orderPopulateData = [
	{ path: 'vendor' },
	{ path: 'customer' },
	{ path: 'rider', select: '_id fullName profileImage' },
	{ path: 'packages', populate: { path: 'meals.meal', select: '_id name image' } },
	{ path: 'statusUpdates', populate: { path: 'updatedBy', select: '_id fullName profileImage' } },
];

export default class OrderService {
	static async getOne(filterQuery) {
		const order = await Order.findOne(filterQuery).populate(orderPopulateData);

		if (!order) return false;
		return order;
	}

	static async count(filterQuery) {
		const count = await Order.countDocuments(filterQuery);
		return count;
	}

	static async getOverallOrderStatistics() {
		const currentMonth = now().startOf('month').toDate();
		const previousMonth = now().subtract(1, 'month').startOf('month').toDate();
		const nextMonth = now().add(1, 'month').startOf('month').toDate();

		const totalOrders = await this.count({});
		const deliveredOrders = await this.count({ status: 'Delivered' });
		const canceledOrders = await this.count({ status: 'Cancelled' });
		const totalRevenueAgg = await Order.aggregate([
			{
				$group: {
					_id: null,
					totalAmount: { $sum: { $add: ['$deliveryInfo.fee', '$total'] } },
				},
			},
		]);
		const totalRevenue = totalRevenueAgg[0]?.totalAmount || 0;

		const lastMonthOrders = await this.count({
			createdAt: {
				$gte: previousMonth,
				$lt: currentMonth,
			},
		});
		const currentMonthOrders = await this.count({
			createdAt: {
				$gte: currentMonth,
				$lt: nextMonth,
			},
		});

		let percentageChange = 0;
		if (lastMonthOrders !== 0) {
			percentageChange = ((currentMonthOrders - lastMonthOrders) / totalOrders) * 100;
		}

		return {
			totalOrders,
			deliveredOrders,
			canceledOrders,
			totalRevenue,
			lastMonthOrders,
			currentMonthOrders,
			percentageChange,
		};
	}

	static async getMany(filterQuery, pageFilter) {
		const options = {
			sort: { createdAt: -1 },
			populate: orderPopulateData,
		};

		if (!pageFilter) return await Order.find(filterQuery, null, options);

		const paging = {
			...options,
			customLabels: paginationLabel,
		};

		if (pageFilter.page) {
			paging.page = pageFilter.page;
		}

		if (pageFilter.limit) {
			paging.limit = pageFilter.limit;
		}

		return await Order.paginate(filterQuery, paging);
	}

	static async getMonthlySales(filterQuery, pageFilter) {
		const dateRange = Utils.getDateRange('monthly');
		filterQuery = {
			...filterQuery,
			createdAt: dateRange,
		};

		return await this.getMany(filterQuery, pageFilter);
	}

	static async getCustomSales(filterQuery, pageFilter) {
		const { startDateStr, endDateStr } = filterQuery.createdAt;
		const dateRange = Utils.getDateRange('', { startDateStr, endDateStr });

		filterQuery = {
			...filterQuery,
			createdAt: dateRange,
		};

		const orders = await this.getMany(filterQuery, pageFilter);
		return orders;
	}

	static async getWeeklySales(filterQuery, pageFilter) {
		const dateRange = Utils.getDateRange('weekly');
		filterQuery = {
			...filterQuery,
			createdAt: dateRange,
		};

		return await this.getMany(filterQuery, pageFilter);
	}

	static async getDailySales(filterQuery, pageFilter) {
		const dateRange = Utils.getDateRange('daily');
		filterQuery = {
			...filterQuery,
			createdAt: dateRange,
		};

		return await this.getMany(filterQuery, pageFilter);
	}

	static async getTotalAmountEarned(user, filterQuery, timeFrame = null) {
		const userType = user.__t.toLowerCase();

		let matchQuery = {
			...filterQuery,
			[`${userType}`]: Types.ObjectId(user._id),
		};

		const dateRange = Utils.getDateRange(timeFrame);

		const result = await Order.aggregate([
			{ $match: { ...matchQuery, createdAt: dateRange } },
			userType == 'rider'
				? { $group: { _id: null, totalAmount: { $sum: '$deliveryInfo.ridersFee' } } }
				: { $group: { _id: null, totalAmount: { $sum: '$total' } } },
		]);

		const totalAmount = result.length > 0 ? result[0].totalAmount : 0;
		return totalAmount;
	}

	static async create(customerId, paymentReference, orderInfo, token) {
		const { vendorId, packages, paymentMethod, deliveryInfo, serviceFee } = orderInfo;

		if (token) {
			const cachedData = await redisClient.get(`new_order:${token}`);
			if (cachedData) throw new SystemError(304, 'Not Processed');

			const order = await this.getOne({ token });
			if (order) throw new ConflictError('Duplicate order token exist');
		}

		if (paymentReference) {
			const order = await this.getOne({ 'payment.reference': paymentReference });
			if (order) throw new ConflictError('Duplicate payment entry');
		}

		const customer = await CustomerService.getOne({ _id: customerId });
		if (!customer) throw new BadRequestError('Invalid customer');

		const vendor = await VendorService.getOne({ _id: vendorId });
		if (!vendor) throw new BadRequestError('Invalid vendor');

		// Validate the meals for each packs and get their total each
		const orderPacks = await Promise.all(
			packages.map(async (packageItem) => {
				const meals = await this.validateMeals(packageItem.meals, vendor);
				const total = meals.reduce((acc, meal) => acc + meal.price, 0);
				return { name: packageItem.name, meals, total };
			})
		);

		// Get the overall total for all the food packages
		const orderTotal = orderPacks.reduce((acc, pack) => acc + pack.total, 0);

		deliveryInfo.fee = deliveryInfo.fee > 0 ? deliveryInfo.fee : 0;
		deliveryInfo.ridersFee = deliveryInfo.fee * (60 / 100);

		const newOrder = Order({
			customer: customerId,
			vendor: vendorId,
			packages: orderPacks,
			total: orderTotal,
			serviceFee: serviceFee || 0,
			deliveryInfo,
			payment: {
				status: paymentMethod === 'Online' ? 'Pending' : 'Unpaid',
				method: paymentMethod,
				...(paymentReference && { reference: paymentReference }),
			},
			token: token || undefined,
		});

		await newOrder.save();

		pushNotificationQueue.addJob({
			user: vendor,
			messageType: 'vendor:order_available_for_process',
		});
		Notification.notify({
			user: vendor,
			messageType: 'vendor:order_available_for_process',
		});
		await this.notifyAvailableRiders();
		const order = await this.getOne({ _id: newOrder._id });

		// OrderService.notifyCustomerSupport(order);
		await redisClient.set(`new_order:${token}`, order, 'EX', 3600); // Add the order to cache
		return order;
	}

	static async initiateOrderPayout(orderId, payoutType) {
		const order = await this.getOne({ _id: orderId });
		if (!order) throw new NotFoundError('Order not found');

		const transactionResponse = await PaystackService.transaction.verify(
			order.payment.reference
		);

		if (!transactionResponse) throw new ForbiddenError("Customer's payment not successful");
		await PayoutService.checkPayout(order, payoutType);

		let user;
		if (payoutType == 'vendor') {
			await OrderService.updatePaymentStatus(order, 'Paid');
			user = await VendorService.getOne({ _id: order.vendor._id });
		} else if (payoutType == 'rider') {
			user = await RiderService.getOne({ _id: order.rider });
			if (!user) throw new ForbiddenError('Order has no rider');

			if (order.status !== 'Delivered')
				throw new ForbiddenError('Order has not been delivered');
		}

		const userPayoutPeriod = user?.payoutSettlement?.period ?? 'Immediate';
		await PayoutService.sharePayout(order, user, userPayoutPeriod);

		return { order };
	}

	static async validateMeals(meals, vendor) {
		return await Promise.all(
			meals.map(async (mealItem) => {
				const meal = await MealService.getOne({
					_id: mongoose.Types.ObjectId(mealItem.meal._id),
				});
				if (!meal) throw new BadRequestError('Invalid Meal Items');
				if (meal.vendor._id.toString() != vendor._id.toString())
					throw new BadRequestError('Some meal items does not exist for this vendor');

				return {
					meal: meal._id,
					quantity: mealItem.quantity,
					price: mealItem.unitPrice * mealItem.quantity,
				};
			})
		);
	}

	static async update(order, updateData, userId) {
		if (Object.keys(updateData).length === 0) {
			throw new BadRequestError('Nothing to update');
		}
		const { riderId, status } = updateData;

		if (status && status !== order.status) {
			order.statusUpdates.push({
				status,
				updatedBy: userId,
				updatedAt: now().toDate(),
			});
		}

		order.status = status ?? order.status;
		order.rider = riderId ?? order.rider;

		await order.save();
		return await this.getOne({ _id: order._id });
	}

	static async updatePaymentStatus(order, newStatus) {
		if (order.payment?.status === newStatus) return;

		if (order.payment?.status === 'Paid') {
			const verified = await PaystackService.transaction.verify(order.paymentRef);
			if (!verified) throw new SystemError(402, 'Payment not successful');
		}

		order.payment.status = newStatus;
		await order.save();

		return await this.getOne({ _id: order._id });
	}

	static async notifyAvailableRiders() {
		const riders = await RiderService.getAvailableRiders();
		await Promise.all(
			riders.map(async (rider) => {
				await pushNotificationQueue.addJob({
					user: rider,
					messageType: 'rider:order_available_for_dispatch',
				});
				Notification.notify({
					user: rider,
					messageType: 'rider:order_available_for_dispatch',
				});
			})
		);
	}

	static async notifyCustomerSupport(order) {
		const emailData = {
			templateName: 'customer_support_new_order',
			recipientEmail: config.CUSTOMER_SUPPORT_EMAIL,
			templateData: { order },
		};
		sendEmail(emailData);
	}

	static async generateRevenueGraph(interval) {
		let groupByFormat;
		if (interval === 'weekly') {
			groupByFormat = '%Y-%m-%d';
		} else if (interval === 'monthly') {
			groupByFormat = '%Y-%m-W';
		} else if (interval === 'yearly') {
			groupByFormat = '%Y-%m';
		} else {
			throw new BadRequestError('Invalid interval');
		}

		const dateRange = Utils.getDateRange(interval);
		const revenueGraphData = await Order.aggregate([
			{
				$match: {
					createdAt: dateRange,
					status: 'Delivered',
					'payment.status': 'Paid',
				},
			},
			{
				$group: {
					_id: {
						$dateToString: {
							format: groupByFormat,
							date: '$createdAt',
						},
					},
					revenue: { $sum: { $add: ['$deliveryInfo.fee', '$total'] } },
				},
			},
			{
				$sort: { _id: 1 },
			},
			{
				$group: {
					_id: null,
					data: { $push: { date: '$_id', revenue: '$revenue' } },
				},
			},
			{
				$project: {
					_id: 0,
					data: { $reverseArray: '$data' },
				},
			},
		]);

		if (revenueGraphData.length === 0) {
			return [];
		}

		const graphData = revenueGraphData[0].data.map((data) => ({
			date: data.date,
			revenue: data.revenue,
			cumulativeRevenue: revenueGraphData[0].cumulativeRevenue,
		}));
		return graphData;
	}

	static async generateOrderToken(user, orderInfo) {
		const expiry = 5 * 60 * 1000;
		try {
			const token = jwt.sign(
				{
					customerId: user._id,
					type: 'order',
					orderInfo,
				},
				config.JWT_PRIVATE_KEY,
				{ expiresIn: expiry }
			);
			const expiresAt = now().add(expiry, 'millisecond');
			return { token, expiresAt };
		} catch (error) {
			throw new SystemError(500, `Failed to generate order token`, error);
		}
	}

	static async decodeOrderToken(token, userId) {
		const decodedToken = jwt.verify(token, config.JWT_PRIVATE_KEY);
		const { type, orderInfo, customerId } = decodedToken;

		// if (now() > dayjs(decodedToken.exp)) throw new UnauthorizedError('Order token expired');

		if (userId && userId != customerId)
			throw new ForbiddenError('Forbidden to create order with this token');

		const customer = CustomerService.getOne({ _id: customerId });
		if (!customer) throw new BadRequestError('Invalid customer');

		if (type != 'order' || !orderInfo) throw new BadRequestError('Invalid order token');

		return decodedToken;
	}
}
