import Device from '../db/models/Device.js';
import { BadRequestError, ConflictError } from '../errors/index.js';
import { paginationLabel } from '../utils/pagination.util.js';

const devicePopulateData = [{ path: 'user', select: '_id fullName profileImage' }];
export default class DeviceService {
	static async getOne(filterQuery) {
		const device = await Device.findOne(filterQuery).populate(devicePopulateData);
		return device || null;
	}

	static async getMany(filterQuery, pageFilter) {
		if (!pageFilter || (!pageFilter.page && !pageFilter.limit)) {
			return await Device.find(filterQuery)
				.sort({ createdAt: -1 })
				.populate(devicePopulateData);
		}

		pageFilter.customLabels = paginationLabel;
		pageFilter.populate = devicePopulateData;
		pageFilter.sort = { createdAt: -1 };

		return await Device.paginate(filterQuery, pageFilter);
	}

	static async registerDevice(user, deviceId, type) {
		const previousDevices = await this.getMany({ user: user._id });
		const maxDevice = 3;
		if (previousDevices.length < maxDevice) {
			const exactUserDevice = await this.getOne({ device_token: deviceId, user: user._id });
			if (exactUserDevice) throw new ConflictError('Device already exists');

			const device = new Device({
				user: user._id,
				device_token: deviceId,
				device_type: type,
			});

			await device.save();
			return await this.getOne({ _id: device._id });
		} else {
			throw new BadRequestError(`You can not have more than ${maxDevice} devices`);
		}
	}

	static async delete(device) {
		const deleted = await Device.findOneAndDelete({ _id: device._id });
		return deleted;
	}
}
