import Meal from '../db/models/Meal.js';
import { BadRequestError, ConflictError } from '../errors/index.js';
import { paginationLabel } from '../utils/pagination.util.js';
import FileService from './FileService.js';
import VendorService from './VendorService.js';

const mealPopulateData = [{ path: 'vendor', select: '_id restaurantName profileImage' }];
export default class MealService {
	static async getOne(filterQuery) {
		const meal = await Meal.findOne(filterQuery).populate(mealPopulateData);
		return meal || null;
	}

	static async getMany(filterQuery, pageFilter) {
		if (!pageFilter || (!pageFilter.page && !pageFilter.limit))
			return await Meal.find(filterQuery).sort({ name: 1 }).populate(mealPopulateData);

		pageFilter.customLabels = paginationLabel;
		pageFilter.populate = mealPopulateData;
		pageFilter.sort = { name: 1 };

		return await Meal.paginate(filterQuery, pageFilter);
	}

	static async create(vendorId, mealData) {
		const vendor = await VendorService.getOne({ _id: vendorId });
		if (!vendor) throw new BadRequestError('Invalid vendor');

		let meal = await this.getOne({ name: mealData.name, vendor: vendorId });
		if (meal) throw new ConflictError(`Meal already exist for ${vendor.restaurantName}`);

		const { name, description, category, type, unitPrice, mealImage, file } = mealData;

		let imageDetails;
		if (mealImage) {
			imageDetails = {
				secure_url: mealImage,
			};
		} else if (file) {
			imageDetails = await FileService.uploadToCloudinary(file, 'meals');
		}

		meal = new Meal({
			name,
			description,
			category,
			type: category === 'Food stack' ? type : undefined,
			unitPrice,
			vendor: vendorId,
			image: { url: imageDetails.secure_url, imageId: imageDetails.public_id },
		});

		await meal.save();
		return await this.getOne({ _id: meal._id });
	}

	static async update(meal, updateData) {
		if (Object.keys(updateData).length === 0) {
			throw new BadRequestError('Nothing to update');
		}
		const { name, description, category, type, unitPrice, mealImage, file, isAvailable } =
			updateData;

		let imageDetails;
		if (mealImage) {
			imageDetails = {
				secure_url: mealImage,
			};
		} else if (file) {
			imageDetails = await FileService.uploadToCloudinary(file, 'meals', meal.image?.imageId);
		}

		meal.name = name ?? meal.name;
		meal.description = description ?? meal.description;
		meal.category = category ?? meal.category;
		meal.type = type ?? meal.type;
		meal.unitPrice = unitPrice ?? meal.unitPrice;
		meal.isAvailable = isAvailable ?? meal.isAvailable;
		meal.image = imageDetails
			? { url: imageDetails.secure_url, imageId: imageDetails.public_id }
			: meal.image;

		await meal.save();
		return meal;
	}

	static async delete(meal) {
		if (meal.image?.imageId) await FileService.deleteFromCloudinary(meal.image.imageId);
		await Meal.deleteOne({ _id: meal._id });
	}
}
