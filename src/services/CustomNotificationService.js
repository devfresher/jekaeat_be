import CustomNotification from '../db/models/CustomNotification.js';
import schedule from 'node-schedule';
import VendorService from './VendorService.js';
import CustomerService from './CustomerService.js';
import RiderService from './RiderService.js';
import UserService from './UserService.js';
import Notification from '../notification/NotificationService.js';
import { ConflictError, NotFoundError, SystemError } from '../errors/index.js';
import winston from 'winston';
import Utils from '../utils/Utils.js';

class CustomNotificationService {
	getOne = async (filterQuery) => {
		const customNotifications = await CustomNotification.findOne(filterQuery);
		return customNotifications;
	};

	getMany = async (filterQuery) => {
		const customNotifications = await CustomNotification.find(filterQuery).sort({
			createdAt: -1,
		});
		return customNotifications;
	};

	create = async (data) => {
		const {
			name,
			time,
			days,
			userType,
			notification: { title, body },
		} = data;
		const label = Utils.getLabel(name);

		if (await this.#labelExists(label)) throw new ConflictError(`${name} already exist`);

		const newSchedule = new CustomNotification({
			name,
			label,
			time,
			days,
			userType,
			notification: { title, body },
		});

		try {
			await newSchedule.save();
			const scheduleId = newSchedule._id.toString();

			const scheduleTimeout = await this.#calculateScheduleTimeOut(days, time);
			schedule.scheduleJob(scheduleId, scheduleTimeout, () => {
				try {
					this.#scheduleHandler(userType, title, body);
				} catch (error) {
					winston.error('Error in scheduled job:', error);
				}
			});
		} catch (error) {
			winston.error('Error creating custom notification:', error);
		}

		return newSchedule;
	};

	#calculateScheduleTimeOut = async (days, time) => {
		const dayOfWeek = days.join(',');
		const timeToSend = time.split(':');
		const hours = timeToSend[0] - 1;
		const minutes = timeToSend[1];
		const scheduleTimeout = `${minutes} ${hours} * * ${dayOfWeek}`;

		return scheduleTimeout;
	};

	rescheduleAll = async () => {
		const customNotifications = await this.getMany({ status: 'active' });
		customNotifications.forEach(async (notificationSchedule) => {
			const {
				time,
				days,
				userType,
				notification: { title, body },
			} = notificationSchedule;

			const scheduleId = notificationSchedule._id.toString();
			const scheduleTimeout = await this.#calculateScheduleTimeOut(days, time);

			schedule.scheduleJob(scheduleId, scheduleTimeout, () => {
				try {
					this.#scheduleHandler(userType, title, body);
				} catch (error) {
					winston.error('Error in scheduled job:', error);
				}
			});
		});

		winston.info(`Rescheduled all active custom notifications`);
		return;
	};

	shutDownReschedule = async () => {
		await schedule.gracefulShutdown();
		winston.info('All scheduled notifications stopped');
	};

	getAll = async (status = '') => {
		let notifications;
		if (status == 'active') {
			const activeNotificationList = schedule.scheduledJobs;
			const keys = Object.keys(activeNotificationList);
			let customNotifications = await this.getMany({});

			notifications = customNotifications.filter((notification) =>
				keys.includes(notification._id.toString())
			);
		} else if (status == 'inactive') {
			notifications = await this.getMany({ status: 'inactive' });
		} else {
			notifications = await this.getMany({});
		}

		return notifications;
	};

	stopScheduledNotification = async (notificationId) => {
		const notification = await this.getOne({ _id: notificationId });
		if (!notification) throw new NotFoundError('Custom Notification not found');

		const activeNotificationList = schedule.scheduledJobs;
		const activeNotification = activeNotificationList[notificationId];
		activeNotification?.cancel();

		if (!activeNotification) throw new ConflictError('Notification currently not active');
		notification.status = 'inactive';
		await notification.save();

		return notification;
	};

	reschedule = async (notificationId, rescheduleData) => {
		const notification = await this.getOne({ _id: notificationId });
		if (!notification) throw new NotFoundError('Custom Notification not found');

		const activeNotificationList = schedule.scheduledJobs;
		let activeNotification = activeNotificationList[notificationId];
		activeNotification?.cancel();

		notification.status = 'active';
		notification.days = rescheduleData.days || notification.days;
		notification.time = rescheduleData.time || notification.time;
		notification.userType = rescheduleData.userType || notification.userType;
		notification.notification = rescheduleData.notification || notification.notification;
		await notification.save();

		const scheduleTimeout = await this.#calculateScheduleTimeOut(
			notification.days,
			notification.time
		);
		activeNotification = schedule.scheduleJob(notificationId, scheduleTimeout, () => {
			try {
				this.#scheduleHandler(
					notification.userType,
					notification.notification.title,
					notification.notification.body
				);
			} catch (error) {
				winston.error('Error in rescheduling job:', error);
			}
		});
		return notification;
	};

	deleteScheduledNotification = async (notificationId) => {
		const notification = await this.getOne({ _id: notificationId });
		if (!notification) throw new NotFoundError('Custom Notification not found');

		const activeNotificationList = schedule.scheduledJobs;
		const activeNotification = activeNotificationList[notificationId];

		await CustomNotification.findByIdAndRemove(notificationId);
		activeNotification?.cancel();

		return notification;
	};

	invokeNotification = async (notificationId) => {
		const notification = await this.getOne({ _id: notificationId });
		if (!notification) throw new NotFoundError('Custom Notification not found');

		const activeNotificationList = schedule.scheduledJobs;
		const activeNotification = activeNotificationList[notificationId];

		if (activeNotification) activeNotification.invoke();
		const {
			userType,
			notification: { title, body },
		} = notification;
		await this.#scheduleHandler(userType, title, body);

		return notification;
	};

	#scheduleHandler = async (userType, title, body) => {
		try {
			let users;
			if (userType == 'Vendor') {
				users = await VendorService.getMany({});
			} else if (userType == 'Customer') {
				users = await CustomerService.getMany({});
			} else if (userType == 'Rider') {
				users = await RiderService.getMany({});
			} else {
				users = await UserService.getMany({});
			}

			const promises = users.map(async (user) => {
				const payload = { user, title, body };
				return await Notification.notify(payload, true);
			});

			await Promise.all(promises);
		} catch (error) {
			winston.error('Error in #scheduleHandler:', error);
			throw error;
		}
	};

	#labelExists = async (label) => {
		const customNotification = await CustomNotification.findOne({ label });
		if (customNotification) return true;

		return false;
	};
}

export default new CustomNotificationService();
