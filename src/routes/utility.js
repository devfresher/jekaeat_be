import { Router } from 'express';
import PayoutController from '../controllers/PayoutController.js';
const router = Router();

router.get('/verify-bank-account', PayoutController.verifyBankAccount);

export default router;
