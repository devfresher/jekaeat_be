import { Router } from "express"
import FileUploadMiddleware from "../../middleware/uploadFiles.js"
import ValidationMiddleware from "../../middleware/validate.js"
import MealController from "../../controllers/MealController.js"

const router = Router()

router.put(
	"/:mealId",
	ValidationMiddleware.validateObjectIds("mealId"),
	FileUploadMiddleware.uploadSingleImage("mealImage"),
	ValidationMiddleware.validateRequest(MealController.validateUpdateMeal),
	MealController.updateMeal
)

router.delete(
	"/:mealId",
	ValidationMiddleware.validateObjectIds("mealId"),
	MealController.deleteMeal
)

export default router
