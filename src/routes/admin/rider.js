import { Router } from 'express';
import RiderController from '../../controllers/RiderController.js';
import ValidationMiddleware from '../../middleware/validate.js';
import CacheMiddleware from '../../middleware/cache.js';

const router = Router();

router.get('/', CacheMiddleware.returnCachedData, RiderController.getAllRiders);

router.get('/stats', CacheMiddleware.returnCachedData, RiderController.getDashboardStats);

router.get(
	'/:riderId',
	ValidationMiddleware.validateObjectIds('riderId'),
	CacheMiddleware.returnCachedData,
	RiderController.getRiderWithStats
);

router.put(
	'/:riderId',
	ValidationMiddleware.validateObjectIds('riderId'),
	ValidationMiddleware.validateRequest(RiderController.validateUpdateProfile),
	RiderController.updateRider
);

export default router;
