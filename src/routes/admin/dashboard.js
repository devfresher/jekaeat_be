import { Router } from 'express';
import StatsController from '../../controllers/StatsController.js';
import CacheMiddleware from '../../middleware/cache.js';

const router = Router();

router.get(
	'/revenue-graph/:interval',
	CacheMiddleware.returnCachedData,
	StatsController.generateRevenueGraph
);

export default router;
