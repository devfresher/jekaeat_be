import { Router } from 'express';
import OrderController from '../../controllers/OrderController.js';
import ValidationMiddleware from '../../middleware/validate.js';
import CacheMiddleware from '../../middleware/cache.js';

const router = Router();

router.get('/', CacheMiddleware.returnCachedData, OrderController.getAllOrders);

router.get('/stats', CacheMiddleware.returnCachedData, OrderController.getDashboardStats);

router.get('/recent-sales', OrderController.getRecentOrders);

router.get(
	'/:orderId',
	ValidationMiddleware.validateObjectIds('orderId'),
	CacheMiddleware.returnCachedData,
	OrderController.getOrder
);

router.post('/initiate-order/:reference', OrderController.initiateOrder);

router.post(
	'/:orderId/initiate-payout',
	ValidationMiddleware.validateObjectIds('orderId'),
	ValidationMiddleware.validateRequest(OrderController.validateInitiateOrderPayout),
	OrderController.initiatePayout
);

export default router;
