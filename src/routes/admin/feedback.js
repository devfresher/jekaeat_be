import { Router } from "express"
import FeedbackController from "../../controllers/FeedbackController.js"
import ValidationMiddleware from "../../middleware/validate.js"
const router = Router()

router.get("/", FeedbackController.getAllFeedbacks)
router.get(
	"/:feedbackId",
	ValidationMiddleware.validateObjectIds("feedbackId"),
	FeedbackController.getFeedback
)

export default router
