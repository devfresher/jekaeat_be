import { Router } from 'express';
import PayoutController from '../../controllers/PayoutController.js';
import ValidationMiddleware from '../../middleware/validate.js';
import CacheMiddleware from '../../middleware/cache.js';

const router = Router();

router.get('/', PayoutController.getAllPayouts);
router.get('/paystack-balance', PayoutController.getPaystackBalance);
router.put(
	'/:payoutId/processPayout',
	ValidationMiddleware.validateObjectIds('payoutId'),
	PayoutController.processPayout
);
export default router;
