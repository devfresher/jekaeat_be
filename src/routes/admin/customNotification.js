import { Router } from 'express';
import { CustomNotificationController } from '../../controllers/CustomNotificationController.js';
import customNotificationValidator from '../../validators/customNotification.validate.js';
import ValidationMiddleware from '../../middleware/validate.js';

class CustomNotificationRouter extends CustomNotificationController {
	constructor() {
		super();
		this.router = Router();
		this.#routes();
	}

	#routes() {
		this.router
			.route('/')
			.post(
				ValidationMiddleware.validateRequest(customNotificationValidator.schedule),
				this.schedule
			)
			.get(this.index);

		this.router
			.route('/:jobId')
			.delete(ValidationMiddleware.validateObjectIds('jobId'), this.delete);

		this.router
			.route('/:jobId/stop')
			.patch(ValidationMiddleware.validateObjectIds('jobId'), this.stop);

		this.router
			.route('/:jobId/restart')
			.patch(
				ValidationMiddleware.validateObjectIds('jobId'),
				ValidationMiddleware.validateRequest(customNotificationValidator.reschedule),
				this.reschedule
			);

		this.router
			.route('/:jobId/invoke')
			.post(ValidationMiddleware.validateObjectIds('jobId'), this.invokeNow);
	}
}

export default new CustomNotificationRouter().router;
