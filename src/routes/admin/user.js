import { Router } from 'express';
import ValidationMiddleware from '../../middleware/validate.js';
import UserController from '../../controllers/UserController.js';
import OrderController from '../../controllers/OrderController.js';
import FileUploadMiddleware from '../../middleware/uploadFiles.js';
import CacheMiddleware from '../../middleware/cache.js';

const router = Router();

router.get(
	'/:userId/orders',
	ValidationMiddleware.validateObjectIds('userId'),
	CacheMiddleware.returnCachedData,
	OrderController.allOrdersOfUser
);

router.post(
	'/',
	ValidationMiddleware.validateRequest(UserController.validateNewUser),
	UserController.createUser
);

router.patch(
	'/:userId/activate',
	ValidationMiddleware.validateObjectIds('userId'),
	UserController.toggleActivateUser
);

router.patch(
	'/:userId/profile-image',
	FileUploadMiddleware.uploadSingleImage('profileImage'),
	ValidationMiddleware.validateRequest(UserController.validateUpdateUserImage),
	UserController.updateUserImage
);

router.delete(
	'/:userId/',
	ValidationMiddleware.validateObjectIds('userId'),
	UserController.deleteUser
);

export default router;
