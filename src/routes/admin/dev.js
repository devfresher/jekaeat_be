import { Router } from 'express';
import { generateAuthUrl, oauth2Client } from '../../mailer/email_providers/gmail.js';
import SettingService from '../../services/SettingService.js';
import encryptor from '../../utils/encryptor.util.js';
import { SystemError } from '../../errors/index.js';

const router = Router();

router.get('/auth/google', (req, res) => {
	const authUrl = generateAuthUrl();
	res.redirect(authUrl);
});

router.get('/auth/google/callback', async (req, res, next) => {
	const { code } = req.query;
	oauth2Client
		.getToken(code)
		.then(async (response) => {
			const { refresh_token, expiry_date: expiresAt, access_token } = response.tokens;
			const { encryptedData: encryptedRefreshToken, iv: encryptionIv } =
				encryptor.encrypt(refresh_token);

			await SettingService.upsert('gmail_auth_token', {
				encryptedRefreshToken,
				encryptionIv,
				expiresAt,
			});
			return res.status(200).send({
				status: 'success',
				data: tokens,
			});
		})
		.catch((err) => {
			const error = new SystemError(500, 'Gmail OAuth error', err);
			next(error);
		});
});

export default router;
