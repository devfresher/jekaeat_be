import { Router } from 'express';
import ValidationMiddleware from '../../middleware/validate.js';
import CustomerController from '../../controllers/CustomerController.js';
import CacheMiddleware from '../../middleware/cache.js';

const router = Router();

router.get('/', CacheMiddleware.returnCachedData, CustomerController.getAllCustomers);

router.get('/stats', CacheMiddleware.returnCachedData, CustomerController.getDashboardStats);

router.get(
	'/:customerId',
	ValidationMiddleware.validateObjectIds('customerId'),
	CacheMiddleware.returnCachedData,
	CustomerController.getCustomerWithStats
);

router.put(
	'/:customerId',
	ValidationMiddleware.validateObjectIds('customerId'),
	ValidationMiddleware.validateRequest(CustomerController.validateUpdateProfile),
	CustomerController.updateCustomer
);

export default router;
