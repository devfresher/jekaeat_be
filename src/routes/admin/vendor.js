import { Router } from 'express';
import VendorController from '../../controllers/VendorController.js';
import ValidationMiddleware from '../../middleware/validate.js';
import MealController from '../../controllers/MealController.js';
import FileUploadMiddleware from '../../middleware/uploadFiles.js';
import CacheMiddleware from '../../middleware/cache.js';

const router = Router();

router.get('/', CacheMiddleware.returnCachedData, VendorController.getAllVendors);

router.get('/stats', CacheMiddleware.returnCachedData, VendorController.getDashboardStats);

router.get('/best-sellers', CacheMiddleware.returnCachedData, VendorController.getBestSellers);

router.get(
	'/:vendorId',
	ValidationMiddleware.validateObjectIds('vendorId'),
	CacheMiddleware.returnCachedData,
	VendorController.getVendorWithStats
);

router.patch(
	'/:vendorId/featured',
	ValidationMiddleware.validateObjectIds('vendorId'),
	VendorController.toggleFeatured
);
router.patch(
	'/:vendorId/toggle-shop-closed',
	ValidationMiddleware.validateObjectIds('vendorId'),
	VendorController.toggleShopClosed
);
router.put(
	'/:vendorId',
	ValidationMiddleware.validateObjectIds('vendorId'),
	ValidationMiddleware.validateRequest(VendorController.validateUpdateProfile),
	VendorController.updateVendor
);

router.get(
	'/:vendorId/meals',
	ValidationMiddleware.validateObjectIds('vendorId'),
	CacheMiddleware.returnCachedData,
	MealController.getVendorsMeal
);

router.post(
	'/:vendorId/meals',
	FileUploadMiddleware.uploadSingleImage('mealImage'),
	ValidationMiddleware.validateRequest(MealController.validateNewMeal),
	MealController.create
);

export default router;
