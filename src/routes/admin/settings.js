import { Router } from 'express';
import SettingController from '../../controllers/SettingController.js';
import ValidationMiddleware from '../../middleware/validate.js';
const router = Router();

router.get('/', SettingController.index);
router.put(
	'/toggle-ordering',
	ValidationMiddleware.validateRequest(SettingController.validateToggleOrdering),
	SettingController.toggleOrdering
);

router.get('/ordering-status', SettingController.getOrderingStatus);

export default router;
