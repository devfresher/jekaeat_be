import { Router } from "express"

import AuthController from "../controllers/AuthController.js"
import ValidationMiddleware from "../middleware/validate.js"
import UserController from "../controllers/UserController.js"

const router = Router()

router.post(
	"/signup",
	ValidationMiddleware.validateRequest(UserController.validateNewUser),
	UserController.createUser
)

router.post(
	"/password_reset/request",
	ValidationMiddleware.validateRequest(AuthController.validateRequestPasswordReset),
	AuthController.requestPasswordReset
)

router.post(
	"/password_reset/validate",
	ValidationMiddleware.validateRequest(AuthController.validateOtpRequest),
	AuthController.handleOTPValidation
)

router.post(
	"/password_reset/reset",
	ValidationMiddleware.validateRequest(AuthController.validateChangePassword),
	AuthController.resetPassword
)

router.post(
	"/login",
	ValidationMiddleware.validateRequest(AuthController.validateSignIn),
	AuthController.signIn
)

export default router
