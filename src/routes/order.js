import { Router } from 'express';

import AuthMiddleware from '../middleware/auth.js';
import ValidationMiddleware from '../middleware/validate.js';

import OrderController from '../controllers/OrderController.js';

import { validUserTypes } from '../db/models/User.js';

const router = Router();

router.post(
	'/new',
	AuthMiddleware.authenticateUserType('Customer'),
	ValidationMiddleware.validateRequest(OrderController.validateOrder),
	OrderController.create
);

router.post(
	'/token',
	AuthMiddleware.authenticateUserType('Customer'),
	ValidationMiddleware.validateRequest(OrderController.validateCreateOrderToken),
	OrderController.createOrderToken
);

router.put(
	'/update-payment-status',
	AuthMiddleware.authenticateUserType('Customer'),
	ValidationMiddleware.validateRequest(OrderController.validateVerifyPaymentStatus),
	OrderController.updatePaymentStatus
);

router.get(
	'/my-orders',
	AuthMiddleware.authenticateUserType(validUserTypes),
	OrderController.myOrders
);

router.get(
	'/my-orders/vendor/:vendorId',
	AuthMiddleware.authenticateUserType(['Rider', 'Customer']),
	ValidationMiddleware.validateObjectIds('vendorId'),
	OrderController.myOrdersByVendor
);

router.get(
	'/dispatch',
	AuthMiddleware.authenticateUserType('Rider'),
	OrderController.OrdersAvailableForDispatch
);

router.get(
	'/my-orders/total',
	AuthMiddleware.authenticateUserType(['Vendor', 'Rider']),
	OrderController.myTotalOrders
);

router.patch(
	'/:orderId/accept',
	AuthMiddleware.authenticateUserType('Rider'),
	ValidationMiddleware.validateObjectIds('orderId'),
	OrderController.acceptDispatchOrder
);

router.patch(
	'/:orderId/ready',
	AuthMiddleware.authenticateUserType('Vendor'),
	ValidationMiddleware.validateObjectIds('orderId'),
	OrderController.setOrderReady
);

router.patch(
	'/:orderId/reject',
	AuthMiddleware.authenticateUserType('Rider'),
	ValidationMiddleware.validateObjectIds('orderId'),
	OrderController.rejectOrder
);

router.patch(
	'/:orderId/status',
	AuthMiddleware.authenticateUserType(['Vendor', 'Rider']),
	ValidationMiddleware.validateObjectIds('orderId'),
	ValidationMiddleware.validateRequest(OrderController.validateUpdateStatus),
	OrderController.updateOrderStatus
);

router.patch(
	'/:orderId/deliver',
	AuthMiddleware.authenticateUserType('Rider'),
	ValidationMiddleware.validateObjectIds('orderId'),
	OrderController.deliverOrder
);

export default router;
