import { Router } from 'express';

import AuthMiddleware from '../middleware/auth.js';
import ValidationMiddleware from '../middleware/validate.js';

import VendorController from '../controllers/VendorController.js';
import { validUserTypes } from '../db/models/User.js';

const router = Router();

router.get('/featured', VendorController.getFeaturedVendors);

router.get(
	'/:vendorId',
	AuthMiddleware.authenticateUserType(validUserTypes),
	ValidationMiddleware.validateObjectIds('vendorId'),
	VendorController.getVendor
);

router.put(
	'/me',
	AuthMiddleware.authenticateUserType('Vendor'),
	ValidationMiddleware.validateRequest(VendorController.validateUpdateProfile),
	VendorController.updateProfile
);

router.patch(
	'/me/toggle-shop-closed',
	AuthMiddleware.authenticateUserType('Vendor'),
	VendorController.toggleShopClosed
);

export default router;
