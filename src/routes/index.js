import authRouter from './auth.js';
import mealRouter from './meal.js';
import orderRouter from './order.js';
import userRouter from './user.js';
import webhookRouter from './webhook.js';
import chatRouter from './chat.js';
import vendorRouter from './vendor.js';
import riderRouter from './rider.js';
import customerRouter from './customer.js';
import feedbackRouter from './feedback.js';
import settingsRouter from './settings.js';
import utilityRouter from './utility.js';

import adminRoutes from './adminRoutes.js';
import { NotFoundError } from '../errors/index.js';

const routeApp = function (app) {
	app.use('/api/auth', authRouter);
	app.use('/api/meals', mealRouter);
	app.use('/api/orders', orderRouter);
	app.use('/api/users', userRouter);
	app.use('/api/customers', customerRouter);
	app.use('/api/riders', riderRouter);
	app.use('/api/vendors', vendorRouter);
	app.use('/api/webhook', webhookRouter);
	app.use('/api/chat', chatRouter);
	app.use('/api/feedbacks', feedbackRouter);
	app.use('/api/settings', settingsRouter);
	app.use('/api/utility', utilityRouter);

	app.use('/api/admin', adminRoutes);

	app.all('*', (req, res, next) => {
		next(
			new NotFoundError(
				`You missed the road. Can not ${req.method} ${req.originalUrl} on this server `
			)
		);
	});
};

export default routeApp;
