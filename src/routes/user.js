import { Router } from 'express';

import AuthMiddleware from '../middleware/auth.js';
import FileUploadMiddleware from '../middleware/uploadFiles.js';
import ValidationMiddleware from '../middleware/validate.js';

import UserController from '../controllers/UserController.js';

import { validUserTypes } from '../db/models/User.js';

const router = Router();

router.put(
	'/change-password',
	AuthMiddleware.authenticateToken,
	ValidationMiddleware.validateRequest(UserController.validateChangePassword),
	UserController.changePassword
);

router.put(
	'/me/settlement-account',
	AuthMiddleware.authenticateUserType(['Vendor', 'Rider']),
	ValidationMiddleware.validateRequest(UserController.validateSettlementAccount),
	UserController.updateSettlementAccount
);

router.patch(
	'/me/profile-image',
	AuthMiddleware.authenticateUserType(validUserTypes),
	FileUploadMiddleware.uploadSingleImage('profileImage'),
	ValidationMiddleware.validateRequest(UserController.validateUpdateUserImage),
	UserController.updateUserImage
);

// router.delete(
// 	"/:userId",
// 	ValidationMiddleware.validateObjectIds("userId"),
// 	UserController.deleteUser
// )

router.post(
	'/devices/new',
	ValidationMiddleware.validateRequest(UserController.validateRegisterDevice),
	UserController.registerDevice
);

router.get(
	'/devices',
	AuthMiddleware.authenticateUserType(validUserTypes),
	UserController.getMyDevices
);

router.delete(
	'/devices/:deviceId/delete',
	AuthMiddleware.authenticateUserType(validUserTypes),
	ValidationMiddleware.validateObjectIds('deviceId'),
	UserController.deleteMyDevice
);

router.put(
	'/me/deactivate',
	AuthMiddleware.authenticateUserType('Customer'),
	UserController.deactivateMyAccount
);

export default router;
