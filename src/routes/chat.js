import { Router } from 'express';

import AuthMiddleware from '../middleware/auth.js';
import ValidationMiddleware from '../middleware/validate.js';

import ChatController from '../controllers/ChatController.js';

import { validUserTypes } from '../db/models/User.js';

const router = Router();

router.post(
	'/send',
	AuthMiddleware.authenticateUserType([...validUserTypes]),
	ValidationMiddleware.validateRequest(ChatController.validateSendMessage),
	ChatController.sendMessage
);

router.get(
	'/',
	AuthMiddleware.authenticateUserType([...validUserTypes]),
	ChatController.getMyChats
);

router.get(
	'/:userId/conversation',
	AuthMiddleware.authenticateUserType([...validUserTypes]),
	ValidationMiddleware.validateObjectIds('userId'),
	ChatController.getConversation
);

router.patch(
	'/read/:messageId',
	AuthMiddleware.authenticateUserType([...validUserTypes]),
	ValidationMiddleware.validateObjectIds('messageId'),
	ChatController.readMessage
);
export default router;
