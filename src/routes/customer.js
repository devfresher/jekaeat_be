import { Router } from "express"

import AuthMiddleware from "../middleware/auth.js"
import ValidationMiddleware from "../middleware/validate.js"

import CustomerController from "../controllers/CustomerController.js"

const router = Router()

router.get(
	"/:customerId",
	ValidationMiddleware.validateObjectIds("customerId"),
	CustomerController.getCustomer
)

router.put(
	"/me",
	AuthMiddleware.authenticateUserType("Customer"),
	ValidationMiddleware.validateRequest(CustomerController.validateUpdateProfile),
	CustomerController.updateProfile
)

export default router
