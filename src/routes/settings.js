import { Router } from 'express';
import SettingController from '../controllers/SettingController.js';
import AuthMiddleware from '../middleware/auth.js';
const router = Router();

router.get(
	'/ordering-status',
	AuthMiddleware.authenticateUserType('Customer'),
	SettingController.getOrderingStatus
);

export default router;
