import { Router } from 'express';

import AuthMiddleware from '../middleware/auth.js';
import ValidationMiddleware from '../middleware/validate.js';

import FeedbackController from '../controllers/FeedbackController.js';
import { validUserTypes } from '../db/models/User.js';

const router = Router();

router.post(
	'/new',
	AuthMiddleware.authenticateUserType([...validUserTypes]),
	ValidationMiddleware.validateRequest(FeedbackController.validateNewFeedback),
	FeedbackController.sendFeedback
);

export default router;
