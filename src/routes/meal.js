import { Router } from 'express';

import AuthMiddleware from '../middleware/auth.js';
import ValidationMiddleware from '../middleware/validate.js';
import FileUploadMiddleware from '../middleware/uploadFiles.js';

import MealController from '../controllers/MealController.js';

const router = Router();

router.post(
	'/new',
	AuthMiddleware.authenticateUserType('Vendor'),
	FileUploadMiddleware.uploadSingleImage('mealImage'),
	ValidationMiddleware.validateRequest(MealController.validateNewMeal),
	MealController.create
);

router.get('/my-meals', AuthMiddleware.authenticateUserType('Vendor'), MealController.myMeals);
router.get('/:mealId', ValidationMiddleware.validateObjectIds('mealId'), MealController.getMeal);
router.put(
	'/:mealId',
	AuthMiddleware.authenticateUserType('Vendor'),
	ValidationMiddleware.validateObjectIds('mealId'),
	FileUploadMiddleware.uploadSingleImage('mealImage'),
	ValidationMiddleware.validateRequest(MealController.validateUpdateMeal),
	MealController.updateMeal
);

router.delete(
	'/:mealId',
	AuthMiddleware.authenticateUserType('Vendor'),
	ValidationMiddleware.validateObjectIds('mealId'),
	MealController.deleteMeal
);

router.get(
	'/vendor/:vendorId',
	// AuthMiddleware.authenticateUserType("Customer"),
	ValidationMiddleware.validateObjectIds('vendorId'),
	MealController.getVendorsMeal
);

export default router;
