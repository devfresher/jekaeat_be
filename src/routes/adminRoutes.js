import { Router } from 'express';

import devRouter from './admin/dev.js';
import dashboardRouter from './admin/dashboard.js';
import authRouter from './admin/auth.js';
import vendorRouter from './admin/vendor.js';
import customerRouter from './admin/customer.js';
import riderRouter from './admin/rider.js';
import orderRouter from './admin/order.js';
import userRouter from './admin/user.js';
import mealRouter from './admin/meal.js';
import payoutRouter from './admin/payout.js';
import settingsRouter from './admin/settings.js';
import feedbackRouter from './admin/feedback.js';
import customNotificationRouter from './admin/customNotification.js';

import AuthMiddleware from '../middleware/auth.js';

const router = Router();

router.use('/auth', authRouter);

// Dev
router.use('/dev', devRouter);

router.use(AuthMiddleware.authenticateUserType('Admin'));

router.use('/dashboard', dashboardRouter);
router.use('/vendors', vendorRouter);
router.use('/customers', customerRouter);
router.use('/riders', riderRouter);
router.use('/orders', orderRouter);
router.use('/users', userRouter);
router.use('/meals', mealRouter);
router.use('/payouts', payoutRouter);
router.use('/settings', settingsRouter);
router.use('/feedbacks', feedbackRouter);
router.use('/custom-notifications', customNotificationRouter);

export default router;
