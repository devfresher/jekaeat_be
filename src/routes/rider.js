import { Router } from "express"

import AuthMiddleware from "../middleware/auth.js"
import ValidationMiddleware from "../middleware/validate.js"

import RiderController from "../controllers/RiderController.js"

const router = Router()

router.get("/available", RiderController.getAvailableRiders)

router.get("/:riderId", ValidationMiddleware.validateObjectIds("riderId"), RiderController.getRider)

router.put(
	"/me",
	AuthMiddleware.authenticateUserType("Rider"),
	ValidationMiddleware.validateRequest(RiderController.validateUpdateProfile),
	RiderController.updateProfile
)

export default router
