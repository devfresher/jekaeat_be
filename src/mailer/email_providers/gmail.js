import nodemailer from 'nodemailer';
import { google } from 'googleapis';
import { config } from '../../utils/config.js';
import settingService from '../../services/SettingService.js';
import encryptor from '../../utils/encryptor.util.js';
import { SystemError } from '../../errors/index.js';
import winston from 'winston';

const oauthConfig = config.GMAIL_OAUTH;
export const oauth2Client = new google.auth.OAuth2(
	oauthConfig.client_id,
	oauthConfig.client_secret,
	oauthConfig.redirect_url
);

export const gmailTransporter = async () => {
	try {
		const gmailAuthToken = await settingService.getOne('gmail_auth_token');
		if (!gmailAuthToken?.encryptedRefreshToken)
			throw new SystemError('GMAIL AUTH TOKEN NOT AVAILABLE');

		let refreshToken = encryptor.decrypt({
			token: gmailAuthToken?.encryptedRefreshToken,
			iv: gmailAuthToken?.encryptionIv,
		});
		let accessToken;

		// Check if the refresh token is expired
		if (!gmailAuthToken?.expiresAt || Date.now() >= gmailAuthToken.expiresAt) {
			oauth2Client.setCredentials({ refresh_token: refreshToken });
			const {
				credentials: { refresh_token, access_token, expiry_date: expiresAt },
			} = await oauth2Client.refreshAccessToken();

			accessToken = access_token;
			const { encryptedData: encryptedRefreshToken, iv: encryptionIv } =
				encryptor.encrypt(refresh_token);

			// Update the auth token
			await settingService.upsert('gmail_auth_token', {
				encryptedRefreshToken,
				encryptionIv,
				expiresAt,
			});
		}

		oauth2Client.setCredentials({ refresh_token: refreshToken });
		accessToken = accessToken || (await oauth2Client.getAccessToken()).token;

		const transporter = nodemailer.createTransport({
			service: 'gmail',
			auth: {
				type: 'OAuth2',
				user: config.ADMIN_EMAIL,
				clientId: oauthConfig.client_id,
				clientSecret: oauthConfig.client_secret,
				refreshToken,
				accessToken,
			},
		});

		return transporter;
	} catch (error) {
		winston.error('Error creating a gmail transporter', error);
		throw error;
	}
};

export const generateAuthUrl = () => {
	return oauth2Client.generateAuthUrl({
		access_type: 'offline',
		prompt: 'consent',
		scope: ['https://mail.google.com/'],
	});
};
