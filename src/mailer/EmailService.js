import mustache from 'mustache';
import fs from 'fs';
import path from 'path';
import { config } from '../utils/config.js';
import { gmailTransporter } from './email_providers/gmail.js';
import { SystemError } from '../errors/index.js';
import winston from 'winston';

const templateSubjectMatch = {
	welcome: {
		subject: 'Welcome to Jekaeat - Healthy Food, Heathy Living, Heathy Live',
		file: 'welcome.html',
	},
	customer_support_new_order: {
		subject: 'New Order Notification',
		file: 'customer_support_new_order.html',
	},
	reset_password: {
		subject: 'Password reset notification',
		file: 'reset_password.html',
	},
	password_reset_successful: {
		subject: 'Password Change Confirmation',
		file: 'password_reset_successful.html',
	},
	order_pickup: {
		subject: 'Your Jekaeat Order Is On Its Way!',
		file: 'order_pickup.html',
	},
	order_confirmed: {
		subject: 'Order confirmed by vendor',
		file: 'order_confirmed.html',
	},
};

export async function sendEmail(data) {
	let { templateName, recipientEmail, templateData } = data;
	const { file, subject } = templateSubjectMatch[templateName];
	const templateDir = path.join(process.cwd(), 'src', 'mailer', 'templates');
	const templatePath = path.join(templateDir, file);

	fs.readFile(templatePath, 'utf8', async (err, templateContent) => {
		if (err) throw err;

		templateData = {
			...templateData,
			appName: config.APP_NAME,
			appLogoUrl: config.APP_LOGO,
		};
		const html = mustache.render(templateContent, templateData);
		const mailOptions = {
			from: config.ADMIN_EMAIL,
			to: recipientEmail,
			subject,
			html,
		};

		gmailTransporter()
			.then(async (googleMail) => {
				await googleMail.sendMail(mailOptions);
				winston.info(`Email (${subject}) sent to ${recipientEmail} by gmail`);
			})
			.catch((error) => {
				winston.error('Error sending email', error);
			});
	});
}
