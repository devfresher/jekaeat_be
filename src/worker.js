import winston from 'winston';
import { sendEmail } from './mailer/EmailService.js';
import { QueueWorker } from './jobs/QueueWorker.js';
import PayoutService from './services/PayoutService.js';
import Notification from './notification/NotificationService.js';
import { connectDB } from './startup/db.js';
import {
	PROCESS_PAYOUT_QUEUE,
	PUSH_NOTIFICATION_QUEUE,
	SEND_EMAIL_QUEUE,
} from './utils/constants.js';

connectDB()
	.then(() => {
		const sendEmailWorker = new QueueWorker(SEND_EMAIL_QUEUE, sendEmail);
		const processPayoutWorker = new QueueWorker(
			PROCESS_PAYOUT_QUEUE,
			PayoutService.processPayout
		);
		const pushNotificationWorker = new QueueWorker(
			PUSH_NOTIFICATION_QUEUE,
			Notification.notify
		);

		sendEmailWorker.start();
		processPayoutWorker.start();
		pushNotificationWorker.start();
	})
	.catch((error) => {
		winston.error(error);
	});
