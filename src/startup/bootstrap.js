import asyncErrors from 'express-async-errors';
import winston from 'winston';
import http from 'http';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';

import logger from './logger.js';
import routeApp from '../routes/index.js';
import socket from './socket.js';
import { connectDB } from '../db/index.js';

import { config } from '../utils/config.js';
import payoutCron from './payouts.js';
import customNotificationService from '../services/CustomNotificationService.js';
import express, { urlencoded } from 'express';
import limitRequestHandler from '../utils/rateLimit.util.js';
import systemMiddleware from '../middleware/response.js';

export default class Server {
	constructor() {
		this.app = express();
		this.port = Number(config.PORT) || 3000;
		this.corsOptions = {
			origin: [],
		};

		logger();
		connectDB()
			.then((connection) => {
				winston.info(`${config.APP_NAME} connected to DB`);

				this.db = connection;
				this.initializeMiddlewaresAndRoutes();

				this.startSocket();
				this.scheduler();

				['SIGINT', 'SIGUSR1', 'SIGUSR2', 'uncaughtException', 'SIGTERM'].forEach(
					(signal) => {
						process.on(signal, async (err) => {
							if (err)
								winston.error(
									`${signal}: Application shutting down due to an unhandled error`,
									err
								);
							this.db.close(() => {
								winston.info('MongoDB connection closed');
								process.exit(0);
							});
						});
					}
				);
			})
			.catch((error) => {
				winston.error(`Error connecting to MongoDB`, error);
				process.exit(0);
			});
	}

	initializeMiddlewaresAndRoutes() {
		this.app.use(bodyParser.json());
		this.app.use(urlencoded({ extended: false }));
		this.app.use(helmet());
		this.app.use(limitRequestHandler());
		this.app.use(cors());

		routeApp(this.app);
		this.app.use(systemMiddleware.response);
	}

	async scheduler() {
		payoutCron();
		await customNotificationService.rescheduleAll();
	}

	startSocket() {
		const server = http.createServer(this.app);
		socket(server);
	}

	start() {
		const { PORT, HOST, APP_NAME } = config;

		this.app.listen(this.port, () => {
			winston.info(`${APP_NAME}'s Server started at http://${HOST}:${PORT}`);
		});
	}
}
