import { Server } from 'socket.io';
import winston from 'winston';

export default (server) => {
	const io = new Server(server, {
		cors: {
			origin: '*',
			methods: ['GET', 'POST'],
		},
	});

	let recipientId, username;
	io.on('connection', (socket) => {
		socket.on('join_room', (data) => {
			recipientId = data.recipient;
			username = data.username;
			socket.join(recipientId);
		});

		socket.on('send_message', (data) => {
			socket.to(recipientId).emit('received_message', data);
			winston.info(`User ${username} sent message to ${recipientId} ${JSON.stringify(data)}`);

			if (data.messageType === 'order') {
				socket.to(recipientId).emit('notification', data);
				winston.info(`Order notification sent to ${recipientId} ${JSON.stringify(data)}`);
			}
		});

		socket.on('disconnect', () => {});
	});
};
