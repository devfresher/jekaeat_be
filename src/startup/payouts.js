import cron from 'node-cron';
import PayoutService from '../services/PayoutService.js';
import { processPayoutQueue } from '../jobs/QueueManager.js';

export default async () => {
	// Daily payout
	const dailyPayout = cron.schedule('0 0 * * *', async () => {
		try {
			const payouts = await PayoutService.getMany({
				processPeriod: 'Daily',
				status: 'Pending',
				processedAt: undefined,
			});

			await PayoutService.processBulkPayouts(payouts);
			await processPayoutQueue.addJob(payouts);
		} catch (error) {
			throw error;
		}
	});

	// Weekly payout
	const weeklyPayout = cron.schedule('0 0 * * 0', async () => {
		try {
			const payouts = await PayoutService.getMany({
				processPeriod: 'Weekly',
				status: 'Pending',
				processedAt: undefined,
			});

			await PayoutService.processBulkPayouts(payouts);
			await processPayoutQueue.addJob(payouts);
		} catch (error) {
			throw error;
		}
	});

	// Monthly payout
	const monthlyPayout = cron.schedule('0 0 1 * *', async () => {
		try {
			const payouts = await PayoutService.getMany({
				processPeriod: 'Weekly',
				status: 'Pending',
				processedAt: undefined,
			});

			await PayoutService.processBulkPayouts(payouts);
			await processPayoutQueue.addJob(payouts);
		} catch (error) {
			throw error;
		}
	});

	dailyPayout.start();
	weeklyPayout.start();
	monthlyPayout.start();
};
