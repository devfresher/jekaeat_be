import { validUserTypes } from '../db/models/User.js';
import { Joi } from '../utils/joi.util.js';
import { validationOption } from './index.js';

class CustomNotificationValidator {
	schedule = async (req) => {
		const schema = Joi.object().keys({
			name: Joi.string().required().label('Custom Notification Name'),
			notification: Joi.object()
				.required()
				.keys({
					title: Joi.string().required().label('Notification Title'),
					body: Joi.string().required().label('Notification Body'),
				})
				.label('Notification'),

			userType: Joi.string()
				.valid(...validUserTypes)
				.label('Target User Type'),
			time: Joi.string().required().label('Time of notification'),
			days: Joi.array().required().items(Joi.number()).label('Days of the week'),
		});

		const validation = schema.validate(req.body, validationOption);
		return validation;
	};

	reschedule = async (req) => {
		const schema = Joi.object().keys({
			name: Joi.string().label('Custom Notification Name'),
			notification: Joi.object()
				.keys({
					title: Joi.string().required().label('Notification Title'),
					body: Joi.string().required().label('Notification Body'),
				})
				.label('Notification'),

			userType: Joi.string()
				.valid(...validUserTypes)
				.label('Target User Type'),
			time: Joi.string().label('Time of notification'),
			days: Joi.array().items(Joi.number()).label('Days of the week'),
		});

		const validation = schema.validate(req.body, validationOption);
		return validation;
	};
}

export default new CustomNotificationValidator();
