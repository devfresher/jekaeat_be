import winston from 'winston';
import PaystackService from '../services/PaystackService.js';
import { config } from '../utils/config.js';
import Utils from '../utils/Utils.js';
import { ForbiddenError } from '../errors/index.js';

export default class WebhookController {
	static async paystack(req, res, next) {
		try {
			const header = req.headers['x-paystack-signature'];
			const payload = req.body;
			winston.info({ payload, signature: req.headers['x-paystack-signature'] });

			if (Utils.isValidSignature(header, payload, config.PAYSTACK_SECRET_KEY)) {
				const { event, data } = req.body;

				const result = await PaystackService.handleWebhook(event, data);

				return res.status(200).send({
					message: 'Webhook handled successfully',
					status: 'success',
					data: result,
				});
			} else {
				throw new ForbiddenError('Invalid signature');
			}
		} catch (error) {
			next(error);
		}
	}
}
