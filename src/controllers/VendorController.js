import { Types } from 'mongoose';
import { Joi } from '../utils/joi.util.js';
import VendorService from '../services/VendorService.js';
import Utils from '../utils/Utils.js';
import CacheMiddleware from '../middleware/cache.js';
import { ForbiddenError, NotFoundError } from '../errors/index.js';

const { daysOfWeek } = Utils;

export default class VendorController {
	static async getAllVendors(req, res, next) {
		try {
			const {
				query: { page, limit },
			} = req;

			const pageFilter = { page, limit };
			const vendors = await VendorService.getManyVendorsWithStats({}, pageFilter);
			CacheMiddleware.cacheResponse(req.originalUrl, vendors);

			return res.status(200).send({
				status: 'success',
				data: vendors,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getBestSellers(req, res, next) {
		try {
			const pageFilter = { page: 1, limit: 10 };
			const vendors = await VendorService.getManyVendorsWithStats(
				{},
				pageFilter,
				true,
				'revenue',
				'desc'
			);
			CacheMiddleware.cacheResponse(req.originalUrl, vendors);

			return res.status(200).send({
				status: 'success',
				data: vendors,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getDashboardStats(req, res, next) {
		try {
			const statistics = await VendorService.getOverallUserStatistics();
			CacheMiddleware.cacheResponse(req.originalUrl, statistics);

			return res.status(200).send({
				status: 'success',
				data: statistics,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getVendor(req, res, next) {
		try {
			const {
				params: { vendorId },
			} = req;

			const vendor = await VendorService.getOne({ _id: vendorId });
			if (!vendor) throw new NotFoundError('Vendor not found');

			return res.status(200).send({
				status: 'success',
				data: vendor,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getVendorWithStats(req, res, next) {
		try {
			const {
				params: { vendorId },
				originalUrl,
			} = req;

			let vendor = await VendorService.getOne({ _id: vendorId });
			if (!vendor) throw new NotFoundError('Vendor not found');

			vendor = await VendorService.getOneVendorWithStats({ _id: vendorId });
			CacheMiddleware.cacheResponse(originalUrl, vendor);

			return res.status(200).send({
				status: 'success',
				data: vendor,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getFeaturedVendors(req, res, next) {
		try {
			const {
				query: { page, limit },
			} = req;

			const pageFilter = { page, limit };
			const featuredVendors = await VendorService.getFeaturedVendors(pageFilter);

			return res.status(200).send({
				status: 'success',
				data: featuredVendors,
			});
		} catch (error) {
			next(error);
		}
	}

	static async toggleFeatured(req, res, next) {
		try {
			const {
				params: { vendorId },
			} = req;

			const vendor = await VendorService.toggleVendorFeatured(vendorId);

			return res.status(200).send({
				status: 'success',
				data: vendor,
			});
		} catch (error) {
			next(error);
		}
	}

	static async toggleShopClosed(req, res, next) {
		try {
			const {
				user: { _id: loggedInUserId, userType },
				params: { vendorId },
			} = req;

			vendorId = userType == 'Admin' ? vendorId : loggedInUserId;
			if (vendorId != Types.ObjectId(loggedInUserId) && userType !== 'Admin')
				throw new ForbiddenError('You are not authorized to close the shop');

			const vendor = await VendorService.toggleShopClosed(vendorId);

			return res.status(200).send({
				status: 'success',
				data: vendor,
			});
		} catch (error) {
			next(error);
		}
	}
	static async updateProfile(req, res, next) {
		try {
			const {
				user: { _id: vendorId },
				body: data,
			} = req;

			let vendor = await VendorService.getOne({ _id: vendorId });
			if (!vendor) throw new NotFoundError('Vendor not found');

			vendor = await VendorService.updateProfile(vendor, data);
			return res.status(200).send({
				status: 'success',
				data: vendor,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updateVendor(req, res, next) {
		try {
			const {
				params: { vendorId },
				body: data,
			} = req;

			let vendor = await VendorService.getOne({ _id: vendorId });
			if (!vendor) throw new NotFoundError('Vendor not found');

			vendor = await VendorService.updateProfile(vendor, data);
			return res.status(200).send({
				status: 'success',
				data: vendor,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateUpdateProfile(req) {
		const validationSchema = Joi.object({
			fullName: Joi.string().trim().messages({
				'string.empty': 'Full name cannot be empty',
			}),
			phoneNumber: Joi.string()
				.regex(/^(\+?234|0)[\d]{10}$/)
				.messages({
					'string.empty': 'Phone Number cannot be empty',
					'string.pattern.base': 'Invalid phone number',
				}),
			restaurantName: Joi.string().trim().messages({
				'string.empty': 'Restaurant cannot be empty',
			}),
			restaurantLocation: Joi.object({
				address: Joi.string().required(),
				city: Joi.string().required(),
				landmark: Joi.object({
					zone: Joi.string(),
					location: Joi.string(),
				}),
			}).messages({
				'string.empty': 'Location cannot be empty',
			}),
			description: Joi.string().trim(),
			schedule: Joi.array().items(
				Joi.object({
					day: Joi.string()
						.valid(...daysOfWeek)
						.required()
						.messages({
							'any.only': `Day must be one of: ${daysOfWeek}`,
							'any.required': 'Day cannot be empty',
						}),
					openAt: Joi.string()
						.pattern(/^([01]\d|2[0-3]):([0-5]\d)$/)
						.required()
						.messages({
							'string.pattern.base':
								'Opening Time must be a string in the format of hh:mm (e.g., 07:45)',
							'any.required': 'Opening Time cannot be empty',
						}),
					closeAt: Joi.string()
						.pattern(/^([01]\d|2[0-3]):([0-5]\d)$/)
						.required()
						.messages({
							'string.pattern.base':
								'Closing Time must be a string in the format of hh:mm (e.g., 22:00)',
							'any.required': 'Closing Time cannot be empty',
						}),
				})
			),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}
}
