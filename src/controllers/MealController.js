import { Types } from 'mongoose';

import { Joi } from '../utils/joi.util.js';

import { mealCategories, mealTypes } from '../db/models/Meal.js';

import MealService from '../services/MealService.js';
import VendorService from '../services/VendorService.js';
import CacheMiddleware from '../middleware/cache.js';
import { ForbiddenError, NotFoundError } from '../errors/index.js';

export default class MealController {
	static async create(req, res, next) {
		try {
			const {
				user: { userType },
			} = req;

			const mealData = req.body;
			mealData.file = req.file;

			let newMeal;
			if (userType === 'Admin') {
				newMeal = await MealService.create(req.params.vendorId, mealData);
			} else {
				newMeal = await MealService.create(req.user._id, mealData);
			}
			return res.status(200).send({
				status: 'success',
				data: newMeal,
			});
		} catch (error) {
			next(error);
		}
	}

	static async myMeals(req, res, next) {
		try {
			const {
				user: { _id: userId },
				query: filter,
			} = req;

			const filterQuery = { vendor: userId };
			if (filter.status === 'available') filterQuery.isAvailable = true;
			if (filter.status === 'unavailable') filterQuery.isAvailable = false;

			const meals = await MealService.getMany(filterQuery, filter);
			return res.status(200).send({
				status: 'success',
				data: meals,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getMeal(req, res, next) {
		try {
			const {
				params: { mealId },
			} = req;

			const meal = await MealService.getOne({ _id: mealId });

			return res.status(200).send({
				status: 'success',
				data: meal,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getVendorsMeal(req, res, next) {
		try {
			const {
				params: { vendorId },
				query: { category, type },
			} = req;

			const vendor = await VendorService.getOne({ _id: vendorId });

			if (!vendor) throw new NotFoundError('Vendor not found');

			let filterQuery = { vendor: vendorId };
			if (category) {
				if (!mealCategories.includes(category))
					throw new NotFoundError('Invalid meal category');

				filterQuery.category = category;
			}

			if (type) {
				if (!mealTypes.includes(type)) throw new NotFoundError('Invalid meal type');
				filterQuery.type = type;
			}

			const meals = await MealService.getMany(filterQuery, req.query);
			CacheMiddleware.cacheResponse(req.originalUrl, meals);

			return res.status(200).send({
				status: 'success',
				data: meals,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updateMeal(req, res, next) {
		try {
			const {
				params: { mealId },
				body: mealData,
				user: { _id: userId, userType },
				file,
			} = req;

			let meal = await MealService.getOne({ _id: mealId });
			if (!meal) throw new NotFoundError('Meal not found');

			mealData.file = file;

			if (meal.vendor._id.toString() != Types.ObjectId(userId) && userType !== 'Admin')
				throw new ForbiddenError('You are not authorized to update this resource');

			meal = await MealService.update(meal, mealData);
			return res.status(200).send({
				status: 'success',
				data: meal,
			});
		} catch (error) {
			next(error);
		}
	}

	static async deleteMeal(req, res, next) {
		try {
			const {
				params: { mealId },
				user: { _id: userId, userType },
			} = req;

			let meal = await MealService.getOne({ _id: mealId });
			if (!meal) throw new NotFoundError('Meal not found');

			if (meal.vendor._id.toString() != Types.ObjectId(userId) && userType !== 'Admin')
				throw new ForbiddenError('You are not authorized to update this resource');

			await MealService.delete(meal);
			return res.status(204).send();
		} catch (error) {
			next(error);
		}
	}

	static validateNewMeal(req) {
		const schema = Joi.object({
			name: Joi.string().trim().required().messages({
				'string.base': 'Name must be a string',
				'string.empty': 'Name cannot be empty',
				'any.required': 'Name is required',
			}),
			mealImage: Joi.string().required().messages({
				'string.empty': 'Meal image cannot be empty',
				'any.required': 'Meal image is required',
			}),
			description: Joi.string().trim().allow('').optional().messages({
				'string.base': 'Description must be a string',
			}),
			category: Joi.string()
				.valid(...mealCategories)
				.required()
				.messages({
					'string.base': 'Category must be a string',
					'string.empty': 'Category cannot be empty',
					'any.only': `Category must be one of ${mealCategories.join(', ')}`,
					'any.required': 'Category is required',
				}),
			type: Joi.when('category', {
				is: 'Food stack',
				then: Joi.string()
					.valid(...mealTypes)
					.required()
					.messages({
						'string.base': 'Type must be a string',
						'string.empty': 'Type cannot be empty',
						'any.only': `Type must be one of ${mealTypes.join(', ')}`,
						'any.required': 'Type is required for the Food stack category',
					}),
				otherwise: Joi.optional().strip(),
			}),
			unitPrice: Joi.number().positive().required().messages({
				'number.base': 'Unit price must be a number',
				'number.empty': 'Unit price cannot be empty',
				'number.positive': 'Unit price must be a positive number',
				'any.required': 'Unit price is required',
			}),
		});

		return schema.validate(req.body, { abortEarly: false });
	}

	static validateUpdateMeal(req) {
		const schema = Joi.object({
			name: Joi.string().trim().messages({
				'string.empty': 'Name cannot be empty',
			}),
			description: Joi.string().trim().allow(''),
			mealImage: Joi.string().messages({
				'string.empty': 'Meal image cannot be empty',
			}),
			category: Joi.string()
				.valid(...mealCategories)
				.messages({
					'string.base': 'Category must be a string',
					'string.empty': 'Category cannot be empty',
					'any.only': `Category must be one of ${mealCategories.join(', ')}`,
					'any.required': 'Category is required',
				}),
			type: Joi.when('category', {
				is: 'Food stack',
				then: Joi.string()
					.valid(...mealTypes)
					.required()
					.messages({
						'string.base': 'Type must be a string',
						'string.empty': 'Type cannot be empty',
						'any.only': `Type must be one of ${mealTypes.join(', ')}`,
						'any.required': 'Type is required for the Food stack category',
					}),
				otherwise: Joi.optional().strip(),
			}),
			isAvailable: Joi.boolean().messages({
				'boolean.base': 'Availability status must be a boolean',
			}),
			unitPrice: Joi.number().messages({
				'number.base': 'Unit price must be a number',
				'number.empty': 'Unit price cannot be empty',
				'number.positive': 'Unit price must be a positive number',
			}),
		});

		return schema.validate(req.body, { abortEarly: false });
	}
}
