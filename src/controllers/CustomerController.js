import { Joi } from '../utils/joi.util.js';

import CustomerService from '../services/CustomerService.js';
import CacheMiddleware from '../middleware/cache.js';
import { NotFoundError } from '../errors/index.js';

export default class CustomerController {
	static async getDashboardStats(req, res, next) {
		try {
			const statistics = await CustomerService.getOverallUserStatistics();
			await CacheMiddleware.cacheResponse(req.originalUrl, statistics);

			return res.status(200).send({
				status: 'success',
				data: statistics,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updateProfile(req, res, next) {
		try {
			const {
				user: { _id: customerId },
			} = req;

			let customer = await CustomerService.getOne({ _id: customerId });
			if (!customer) throw new NotFoundError('Customer not found');

			customer = await CustomerService.updateProfile(customer, req.body);
			return res.status(200).send({
				message: 'Profile updated',
				status: 'success',
				data: customer,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updateCustomer(req, res, next) {
		try {
			const {
				params: { customerId },
			} = req;

			let customer = await CustomerService.getOne({ _id: customerId });
			if (!customer) throw new NotFoundError('Customer not found');

			customer = await CustomerService.updateProfile(customer, req.body);
			return res.status(200).send({
				message: 'Customer profile updated',
				status: 'success',
				data: customer,
			});
		} catch (error) {
			next(error);
		}
	}

	static async countAll(req, res, next) {
		try {
			const count = await CustomerService.count({});
			return res.status(200).send({
				message: 'Customer count retrieved',
				status: 'success',
				data: { count },
			});
		} catch (error) {
			next(error);
		}
	}

	static async getCustomer(req, res, next) {
		try {
			const {
				params: { customerId },
			} = req;

			const customer = await CustomerService.getOne({ _id: customerId });

			if (!customer) throw new NotFoundError('Customer not found');
			return res.status(200).send({
				message: 'Customer info retrieved',
				status: 'success',
				data: customer,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getCustomerWithStats(req, res, next) {
		try {
			const {
				params: { customerId },
			} = req;

			let customer = await CustomerService.getOne({ _id: customerId });
			if (!customer) throw new NotFoundError('Customer not found');

			customer = await CustomerService.getOneCustomerWithStats({ _id: customerId });
			await CacheMiddleware.cacheResponse(req.originalUrl, customer);

			return res.status(200).send({
				status: 'success',
				data: customer,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getAllCustomers(req, res, next) {
		try {
			const customers = await CustomerService.getManyCustomersWithStats({}, req.query);
			await CacheMiddleware.cacheResponse(req.originalUrl, customers);

			return res.status(200).send({
				status: 'success',
				data: customers,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateUpdateProfile(req) {
		const validationSchema = Joi.object({
			fullName: Joi.string().trim().messages({
				'string.empty': 'Full name cannot be empty',
			}),
			phoneNumber: Joi.string()
				.regex(/^(\+?234|0)[\d]{10}$/)
				.messages({
					'string.empty': 'Phone Number cannot be empty',
					'string.pattern.base': 'Invalid phone number',
				}),
			deliveryAddress: Joi.object({
				address: Joi.string().required(),
				city: Joi.string(),
				nearestLandmark: Joi.string(),
				country: Joi.string(),
				contactName: Joi.string().required(),
				contactPhone: Joi.string().required(),
			}).messages({
				'string.empty': 'Address cannot be empty',
			}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}
}
