import { Types } from 'mongoose';

import { Joi } from '../utils/joi.util.js';

import { orderStatus, paymentMethods } from '../db/models/Order.js';

import OrderService from '../services/OrderService.js';
import PaystackService from '../services/PaystackService.js';
import UserService from '../services/UserService.js';
import RiderService from '../services/RiderService.js';
import VendorService from '../services/VendorService.js';
import PayoutService from '../services/PayoutService.js';
import Notification from '../notification/NotificationService.js';
import { emailQueue, pushNotificationQueue } from '../jobs/QueueManager.js';
import { sendEmail } from '../mailer/EmailService.js';
import CustomerService from '../services/CustomerService.js';
import { BadRequestError, ForbiddenError, NotFoundError } from '../errors/index.js';
import CacheMiddleware from '../middleware/cache.js';

export default class OrderController {
	static async getAllOrders(req, res, next) {
		try {
			const {
				query: { period, startDate, endDate, status, page, limit },
			} = req;

			const filterQuery = {};
			const pageFilter = { page, limit };

			if (status) filterQuery.status = status;

			let orders;
			switch (period) {
				case 'daily':
					orders = await OrderService.getDailySales(filterQuery, pageFilter);
					break;
				case 'weekly':
					orders = await OrderService.getWeeklySales(filterQuery, pageFilter);
					break;
				case 'monthly':
					orders = await OrderService.getMonthlySales(filterQuery, pageFilter);
					break;
				case 'custom':
					if (startDate && endDate)
						filterQuery.createdAt = { startDateStr: startDate, endDateStr: endDate };

					orders = await OrderService.getCustomSales(filterQuery, pageFilter);
					break;
				default:
					orders = await OrderService.getMany(filterQuery, pageFilter);
					break;
			}

			CacheMiddleware.cacheResponse(req.originalUrl, orders);
			res.status(200).send({
				status: 'success',
				data: orders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getDashboardStats(req, res, next) {
		try {
			const statistics = await OrderService.getOverallOrderStatistics();

			CacheMiddleware.cacheResponse(req.originalUrl, statistics);
			return res.status(200).send({
				status: 'success',
				data: statistics,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getRecentOrders(req, res, next) {
		try {
			const {
				query: { status },
			} = req;

			const filterQuery = {};
			const pageFilter = { page: 1, limit: 10 };

			if (status) filterQuery.status = status;

			const orders = await OrderService.getMany(filterQuery, pageFilter);
			return res.status(200).send({
				status: 'success',
				data: orders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async createOrderToken(req, res, next) {
		try {
			const {
				user: { _id: customerId },
				body: { vendorId, packages, paymentMethod, deliveryInfo, serviceFee },
			} = req;

			const vendor = await VendorService.getOne({ _id: vendorId });
			if (!vendor) throw new BadRequestError('Vendor does not exist');

			const orderInfo = {
				vendorId,
				packages,
				paymentMethod,
				deliveryInfo,
				serviceFee,
			};

			const customer = await CustomerService.getOne({ _id: customerId });
			if (!customer) throw new BadRequestError('User does not exist');

			const orderToken = await OrderService.generateOrderToken(customer, orderInfo);
			return res.status(201).send({
				message: 'Order token generated',
				status: 'success',
				data: orderToken,
			});
		} catch (error) {
			next(error);
		}
	}

	static async create(req, res, next) {
		try {
			const {
				body: { paymentReference, token },
				user: { _id: userId },
			} = req;

			const { orderInfo, customerId } = await OrderService.decodeOrderToken(token, userId);
			const newOrder = await OrderService.create(
				customerId,
				paymentReference,
				orderInfo,
				token
			);

			return res.status(201).send({
				message: 'Order Created',
				status: 'success',
				data: newOrder,
			});
		} catch (error) {
			next(error);
		}
	}

	static async initiateOrder(req, res, next) {
		try {
			const {
				params: { reference },
			} = req;

			const { status, data } = await PaystackService.transaction.verify(reference);
			if (!status) throw new BadRequestError('Unable to resolve payment reference');

			const order = await PaystackService.handleChargeSuccess(data);

			await pushNotificationQueue.addJob({
				user: order.customer,
				messageType: 'customer:payment_confirmed_success',
			});
			Notification.notify({
				user: order.customer,
				messageType: 'customer:payment_confirmed_success',
			});

			const emailData = {
				templateName: 'order_confirmed',
				recipientEmail: order.customer.email,
				templateData: {
					order,
					user: order.customer,
					vendor: order.vendor,
				},
			};
			emailQueue.addJob(emailData);
			sendEmail(emailData);

			return res.status(200).send({
				status: 'success',
				message: 'Order Initiated',
			});
		} catch (error) {
			next(error);
		}
	}

	static async initiatePayout(req, res, next) {
		try {
			const {
				params: { orderId },
				body: { payoutType },
			} = req;

			const order = await OrderService.initiateOrderPayout(orderId, payoutType);
			return res.status(200).send({
				status: 'success',
				message: 'Payout Initiated',
				data: order,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updatePaymentStatus(req, res, next) {
		try {
			const {
				body: { paymentReference },
			} = req;

			const order = await OrderService.getOne({ 'payment.reference': paymentReference });
			if (!order) throw new BadRequestError('Invalid payment reference');

			const payment = await PaystackService.transaction.verify(paymentReference);
			if (!payment.status) throw new BadRequestError('Unable to resolve payment reference');

			const data = payment.data;
			const status = data.status;

			switch (status) {
				case 'success':
					PaystackService.handleChargeSuccess(data);
					await pushNotificationQueue.addJob({
						user: order.customer,
						messageType: 'customer:payment_confirmed_success',
					});
					Notification.notify({
						user: order.customer,
						messageType: 'customer:payment_confirmed_success',
					});
					const emailData = {
						templateName: 'order_confirmed',
						recipientEmail: order.customer.email,
						templateData: {
							order,
							user: order.customer,
							vendor: order.vendor,
						},
					};
					emailQueue.addJob(emailData);
					sendEmail(emailData);
					break;
				case 'failed':
					PaystackService.handleChargeFailed(data);
					await pushNotificationQueue.addJob({
						user: order.customer,
						messageType: 'customer:payment_confirmed_failed',
					});
					Notification.notify({
						user: order.customer,
						messageType: 'customer:payment_confirmed_failed',
					});

					break;
			}

			return res.status(200).send({
				status: 'success',
				data: order,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getOrder(req, res, next) {
		try {
			const { orderId } = req.params;

			const order = await OrderService.getOne({ _id: orderId });
			if (!order) throw new NotFoundError('Order not found');

			CacheMiddleware.cacheResponse(req.originalUrl, order);
			res.status(200).send({
				status: 'success',
				data: order,
			});
		} catch (error) {
			next(error);
		}
	}

	static async myOrders(req, res, next) {
		try {
			const {
				user: { _id: userId, userType },
				query: { status, page, limit },
			} = req;

			const pageFilter = { page, limit };
			const userTypeLower = userType.toLowerCase();

			const filterQuery = { [`${userTypeLower}`]: userId };
			if (status) filterQuery.status = status;

			const orders = await OrderService.getMany(filterQuery, pageFilter);
			return res.status(200).send({
				status: 'success',
				data: orders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async myOrdersByVendor(req, res, next) {
		try {
			const {
				params: { vendorId },
				user: { _id: userId, userType },
				query: { page, limit },
			} = req;

			let userTypeLower = userType.toLowerCase();
			const pageFilter = { page, limit };

			const vendor = await VendorService.getOne({ _id: vendorId });
			if (!vendor) throw new NotFoundError('Vendor not found');

			const filterQuery = {
				[`${userTypeLower}`]: Types.ObjectId(userId),
				vendor: vendor._id,
			};
			const orders = await OrderService.getMany(filterQuery, pageFilter);
			return res.status(200).send({
				status: 'success',
				data: orders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async allOrdersOfUser(req, res, next) {
		try {
			const {
				query: { period, startDate, endDate, status, page, limit },
				params: { userId },
			} = req;

			let filterQuery = {};
			const pageFilter = { page, limit };

			const user = await UserService.getOne({ _id: userId });
			if (!user) throw new NotFoundError('User not found');

			if (status) filterQuery.status = status;
			filterQuery[`${user.__t.toLowerCase()}`] = user._id;

			let orders;
			switch (period) {
				case 'daily':
					orders = await OrderService.getDailySales(filterQuery, pageFilter);
					break;
				case 'weekly':
					orders = await OrderService.getWeeklySales(filterQuery, pageFilter);
					break;
				case 'monthly':
					orders = await OrderService.getMonthlySales(filterQuery, pageFilter);
					break;
				case 'custom':
					if (startDate && endDate)
						filterQuery.createdAt = { startDateStr: startDate, endDateStr: endDate };

					orders = await OrderService.getCustomSales(filterQuery, pageFilter);
					break;
				default:
					orders = await OrderService.getMany(filterQuery, pageFilter);
					break;
			}

			CacheMiddleware.cacheResponse(req.originalUrl, orders);
			res.status(200).send({
				status: 'success',
				data: orders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async OrdersAvailableForDispatch(req, res, next) {
		try {
			const {
				user: { _id: userId },
				query: { page, limit },
			} = req;

			const pageFilter = { page, limit };
			const rider = await RiderService.getOne({ _id: userId });
			if (!rider.payoutSettlement || !(await UserService.isPayoutAccountComplete(rider)))
				throw new ForbiddenError(
					'Incomplete Profile: Complete your profile to access orders for dispatch'
				);

			const availableOrders = await OrderService.getMany(
				{ status: { $in: ['Pending', 'Ready'] } },
				pageFilter
			);

			return res.status(200).send({
				message: 'Dispatch ready orders retrieved!',
				status: 'success',
				data: availableOrders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async setOrderReady(req, res, next) {
		try {
			const {
				params: { orderId },
			} = req;

			let order = await OrderService.getOne({ _id: orderId });
			if (!order) throw new NotFoundError('Order not found');

			if (order.vendor._id.toString() != Types.ObjectId(req.user._id))
				throw new ForbiddenError('You are not authorized to update this resource');

			if (order.status !== 'Pending' && order.status !== 'Ready')
				throw new NotFoundError(`Order currently ${order.status}`);

			order = await OrderService.update(order, { status: 'Ready' }, req.user._id);

			await OrderService.notifyAvailableRiders();
			await pushNotificationQueue.addJob({
				user: order.customer,
				messageType: 'customer:order_ready_for_delivery',
			});
			Notification.notify({
				user: order.customer,
				messageType: 'customer:order_ready_for_delivery',
			});

			return res.status(200).send({
				status: 'success',
				data: order,
			});
		} catch (error) {
			next(error);
		}
	}

	static async acceptDispatchOrder(req, res, next) {
		try {
			const {
				user: { _id: riderId },
				params: { orderId },
			} = req;

			const order = await OrderService.getOne({ _id: orderId });

			if (!order) throw new NotFoundError('Order not found');
			if (order.status !== 'Ready' && order.status !== 'Pending')
				throw new BadRequestError('Order not available for dispatch');

			const updatedOrder = await OrderService.update(
				order,
				{ status: 'Accepted', riderId },
				riderId
			);

			await pushNotificationQueue.addJob({
				user: order.customer,
				messageType: 'customer:order_accepted_for_dispatch',
			});
			Notification.notify({
				user: order.customer,
				messageType: 'customer:order_accepted_for_dispatch',
			});
			return res.status(200).send({
				status: 'success',
				data: updatedOrder,
			});
		} catch (error) {
			next(error);
		}
	}

	static async rejectOrder(req, res, next) {
		try {
			const {
				params: { orderId },
			} = req;

			let order = await OrderService.getOne({ _id: orderId });
			if (!order) throw new NotFoundError('Order not found');

			if (order.rider._id.toString() != Types.ObjectId(req.user._id))
				throw new ForbiddenError('You are not authorized to reject this order');

			if (order.status !== 'Ready')
				order = await OrderService.update(order, { status: 'Ready' }, req.user._id);

			OrderService.notifyAvailableRiders();
			await pushNotificationQueue.addJob({
				user: order.vendor,
				messageType: 'vendor:order_rejected_by_rider',
			});
			Notification.notify({
				user: order.vendor,
				messageType: 'vendor:order_rejected_by_rider',
			});

			return res.status(200).send({
				status: 'success',
				data: order,
			});
		} catch (error) {
			next(error);
		}
	}

	static async deliverOrder(req, res, next) {
		try {
			const {
				params: { orderId },
			} = req;

			let order = await OrderService.getOne({ _id: orderId });
			if (!order) throw new NotFoundError('Order not found');

			const rider = await RiderService.getOne({ _id: order.rider._id });
			const riderPayoutPeriod = rider?.payoutSettlement?.period ?? 'Daily';

			if (rider._id.toString() != Types.ObjectId(req.user._id))
				throw new ForbiddenError('You are not authorized to deliver this order');

			order = await OrderService.update(order, { status: 'Delivered' }, rider._id);

			await PayoutService.sharePayout(order, rider, riderPayoutPeriod);

			await pushNotificationQueue.addJob({
				user: order.customer,
				messageType: 'customer:order_delivered',
			});
			Notification.notify({
				user: order.customer,
				messageType: 'customer:order_delivered',
			});
			await pushNotificationQueue.addJob({
				user: order.vendor,
				messageType: 'vendor:order_delivered',
			});
			Notification.notify({
				user: order.vendor,
				messageType: 'vendor:order_delivered',
			});

			return res.status(200).send({
				status: 'success',
				data: order,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updateOrderStatus(req, res, next) {
		try {
			const {
				user: { _id: userId },
				body: { status },
				params: { orderId },
			} = req;

			if (status === 'Delivered')
				throw new BadRequestError(
					'You can not deliver through this endpoint use {orderId}/deliver'
				);

			const order = await OrderService.getOne({ _id: orderId });
			if (!order) throw new NotFoundError('Order not found');

			const updatedOrder = await OrderService.update(order, { status }, userId);

			// Notify customer for dispatch
			if (status === 'Dispatching') {
				const emailData = {
					templateName: 'order_pickup',
					recipientEmail: order.customer.email,
					templateData: {
						user: order.customer,
						vendor: order.vendor,
					},
				};
				await pushNotificationQueue.addJob({
					user: order.customer,
					messageType: 'customer:order_dispatching',
				});
				Notification.notify({
					user: order.customer,
					messageType: 'customer:order_dispatching',
				});
				emailQueue.addJob(emailData);
				sendEmail(emailData);
			}

			return res.status(200).send({
				status: 'status',
				data: updatedOrder,
				message: 'Order status updated',
			});
		} catch (error) {
			next(error);
		}
	}

	static async myTotalOrders(req, res, next) {
		try {
			const {
				query: { timeFrame, status },
				user: { _id: userId },
			} = req;

			let filterQuery = {};

			const user = await UserService.getOne({ _id: userId });

			if (status && !orderStatus.includes(status))
				throw new BadRequestError('Invalid filter with status');

			if (status) filterQuery.status = status;

			const total = await OrderService.getTotalAmountEarned(user, filterQuery, timeFrame);

			return res.status(200).send({
				status: 'success',
				data: total,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateOrder(req) {
		const schema = Joi.object({
			token: Joi.string().required(),
			paymentMethod: Joi.string()
				.valid(...paymentMethods)
				.required(),
			paymentReference: Joi.when('paymentMethod', {
				is: 'Online',
				then: Joi.string().required(),
				otherwise: Joi.forbidden(),
			}),
		});

		return schema.validate(req.body);
	}

	static validateInitiateOrderPayout(req) {
		const schema = Joi.object({
			payoutType: Joi.string()
				.required()
				.valid(...['vendor', 'rider'])
				.label('Payout Type'),
		});

		return schema.validate(req.body);
	}

	static validateCreateOrderToken(req) {
		const schema = Joi.object({
			vendorId: Joi.objectId().required().label('Vendor'),
			serviceFee: Joi.number().min(0).label('Service Fee'),
			packages: Joi.array()
				.items(
					Joi.object({
						name: Joi.string().trim(),
						meals: Joi.array().items(
							Joi.object({
								meal: Joi.object().required(),
								quantity: Joi.number().min(1).required(),
								unitPrice: Joi.number().min(0).required(),
							})
						),
					}).label('Package')
				)
				.required()
				.label('Packages'),
			paymentMethod: Joi.string()
				.valid(...paymentMethods)
				.required(),
			deliveryInfo: Joi.object({
				fee: Joi.number().positive().optional().label('Delivery fee'),
				address: Joi.string().required(),
				nearestLandmark: Joi.string().allow(''),
				deliveryInstructions: Joi.string().allow(''),
				contactName: Joi.string().required(),
				contactPhone: Joi.string()
					.regex(/^(\+?234|0)[\d]{10}$/)
					.required(),
			}).required(),
		});

		return schema.validate(req.body);
	}

	static validateVerifyPaymentStatus(req) {
		const schema = Joi.object({
			paymentReference: Joi.string().required(),
		});

		return schema.validate(req.body);
	}

	static validateUpdateStatus(req) {
		const schema = Joi.object({
			status: Joi.string()
				.valid(...orderStatus)
				.required(),
		});

		return schema.validate(req.body);
	}
}
