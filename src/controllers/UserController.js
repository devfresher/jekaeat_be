import { Joi } from '../utils/joi.util.js';

import { periodicPayoutSettlement, validUserTypes } from '../db/models/User.js';

import UserService from '../services/UserService.js';
import DeviceService from '../services/DeviceService.js';
import VendorService from '../services/VendorService.js';
import RiderService from '../services/RiderService.js';
import CustomerService from '../services/CustomerService.js';
import { emailQueue } from '../jobs/QueueManager.js';
import { sendEmail } from '../mailer/EmailService.js';
import { BadRequestError, ConflictError, ForbiddenError, NotFoundError } from '../errors/index.js';
import { validRideTypes, validRiderTypes } from '../db/models/Rider.js';

export default class UserController {
	static async createUser(req, res, next) {
		try {
			const { body: data } = req;

			let userData, user;
			if (data.type === 'Customer') {
				userData = await CustomerService.create(data);
				user = userData.newCustomer;
			} else if (data.type === 'Vendor') {
				userData = await VendorService.create(data);
				user = userData.newVendor;
			} else if (data.type === 'Rider') {
				userData = await RiderService.create(data);
				user = userData.newRider;
			} else {
				throw new BadRequestError('Invalid user type');
			}

			const emailData = {
				templateName: 'welcome',
				recipientEmail: user.email,
				templateData: { user },
			};

			emailQueue.addJob(emailData);
			sendEmail(emailData);

			return res.status(200).send({
				status: 'success',
				data: userData,
			});
		} catch (error) {
			next(error);
		}
	}

	static async changePassword(req, res, next) {
		try {
			const {
				body: { currentPassword, newPassword },
			} = req;

			if (currentPassword === newPassword)
				throw new ConflictError(
					'Unable to update password. Please ensure that the current password and new password are not the same'
				);

			const user = await UserService.changeUserPassword(
				req.user._id,
				currentPassword,
				newPassword
			);

			return res.status(200).send({
				status: 'success',
				data: user,
			});
		} catch (error) {
			next(error);
		}
	}

	static async deactivateMyAccount(req, res, next) {
		try {
			const {
				user: { _id: userId },
			} = req;

			let user = await UserService.getOne({ _id: userId });
			if (!user) throw new NotFoundError('User not found');

			user = await UserService.toggleUserActivation(user, false);
			return res.status(200).send({
				status: 'success',
				data: user,
			});
		} catch (error) {
			next(error);
		}
	}

	static async deleteUser(req, res, next) {
		try {
			const {
				params: { userId },
			} = req;

			const user = await UserService.getOne({ _id: userId });
			if (!user) throw new NotFoundError('User not found');

			await UserService.delete(user);
			return res.status(204).send();
		} catch (error) {
			next(error);
		}
	}

	static async updateSettlementAccount(req, res, next) {
		try {
			const {
				user: { _id: userId },
			} = req;

			let user = await UserService.getOne({ _id: userId });
			if (!user) throw new BadRequestError('Invalid user Id');

			user = await UserService.updateSettlementAccount(user, req.body);

			return res.status(200).send({
				status: 'success',
				data: user,
			});
		} catch (error) {
			next(error);
		}
	}

	static async registerDevice(req, res, next) {
		try {
			const {
				body: {
					userId,
					device: { deviceId, deviceType },
				},
			} = req;

			const user = await UserService.getOne({ _id: userId });
			if (!user) throw new BadRequestError('Invalid user');

			const device = await DeviceService.registerDevice(user, deviceId, deviceType);
			return res.status(200).send({
				status: 'success',
				data: device,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getMyDevices(req, res, next) {
		try {
			const {
				user: { _id: userId },
			} = req;

			const user = await UserService.getOne({ _id: userId });
			if (!user) throw new BadRequestError('Invalid user');

			const devices = await DeviceService.getMany({ user: user._id });
			return res.status(200).send({
				status: 'success',
				data: devices,
			});
		} catch (error) {
			next(error);
		}
	}

	static async deleteMyDevice(req, res, next) {
		try {
			const {
				user: { _id: userId },
				params: { deviceId },
			} = req;

			const user = await UserService.getOne({ _id: userId });
			if (!user) throw new BadRequestError('Invalid user');

			const device = await DeviceService.getOne({ _id: deviceId });
			if (!device) throw new NotFoundError('Device not found');

			if (device.user._id != userId)
				throw new ForbiddenError('You are not authorized to delete this device');

			await DeviceService.delete(device);
			return res.status(204).send();
		} catch (error) {
			next(error);
		}
	}

	static async updateUserImage(req, res, next) {
		try {
			const {
				user: { _id: loggedInUserId, userType: loggedInUserType },
				params: { userId },
				body: profileImageData,
				file,
			} = req;
			userId = loggedInUserType === 'Admin' ? userId : loggedInUserId;

			let user = await UserService.getOne({ _id: userId });
			if (!user) throw new NotFoundError('User not found');

			profileImageData.file = file;

			user = await UserService.updateProfileImage(user, profileImageData);
			return res.status(200).send({
				status: 'success',
				data: user,
			});
		} catch (error) {
			next(error);
		}
	}

	static async toggleActivateUser(req, res, next) {
		try {
			const {
				params: { userId },
			} = req.params;

			let user = await UserService.getOne({ _id: userId });
			if (!user) throw new NotFoundError('User not found');

			user = await UserService.toggleUserActivation(user);

			return res.status(200).send({
				status: 'success',
				data: user,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateChangePassword(req) {
		const validationSchema = Joi.object({
			currentPassword: Joi.string().required().trim().messages({
				'string.base': 'Current password must be a string',
				'any.required': 'Current password is required',
			}),
			newPassword: Joi.string().required().trim().messages({
				'string.base': 'New password must be a string',
				'any.required': 'New password is required',
			}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateRegisterDevice(req) {
		const validationSchema = Joi.object({
			userId: Joi.string().required().trim().messages({
				'string.base': 'User ID must be a string',
				'any.required': 'User ID is required',
			}),
			device: Joi.object({
				deviceId: Joi.string().required().trim().messages({
					'string.base': 'Device ID must be a string',
					'any.required': 'Device ID is required',
				}),
				deviceType: Joi.string()
					.valid(...['Android', 'IOS'])
					.trim()
					.messages({
						'string.base': 'Device Type must be a string',
					}),
			}).required(),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateSettlementAccount(req) {
		const { userType } = req.user;
		const commonValidationOptions = {
			'string.base': '{#label} must be a string',
			'string.empty': '{#label} is not allowed to be empty',
		};

		const validationSchemas = {
			Vendor: Joi.object({
				accountNumber: Joi.string().trim().messages(commonValidationOptions),
				accountName: Joi.string().trim().messages(commonValidationOptions),
				bankCode: Joi.string().trim().messages(commonValidationOptions),
			}),
			Rider: Joi.object({
				accountNumber: Joi.string().trim().messages(commonValidationOptions),
				accountName: Joi.string().trim().messages(commonValidationOptions),
				bankCode: Joi.string().trim().messages(commonValidationOptions),
				allowPeriodicPayout: Joi.boolean().messages({
					'boolean.base': '{#label} must be a boolean value',
				}),
				period: Joi.string()
					.valid(...periodicPayoutSettlement)
					.messages({
						'any.only': `Type must be one of ${periodicPayoutSettlement.join(', ')}`,
					}),
			}),
		};

		const validationSchema = validationSchemas[userType];
		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateNewUser(req) {
		const validationSchema = Joi.object({
			fullName: Joi.string().required().trim().messages({
				'string.empty': 'Full name is required',
			}),
			email: Joi.string().email().required().trim().lowercase().messages({
				'string.empty': 'Email is required',
				'string.email': 'Please provide a valid email address',
			}),
			password: Joi.string().min(6).required().trim().messages({
				'string.empty': 'Password is required',
				'string.min': 'Password should be at least 6 characters long',
			}),
			phoneNumber: Joi.string()
				.regex(/^(\+?234|0)[\d]{10}$/)
				.required()
				.trim()
				.messages({
					'string.empty': 'Phone number is required',
					'string.pattern.base': 'Invalid phone number',
				}),
			type: Joi.string()
				.valid(...validUserTypes)
				.trim()
				.required()
				.messages({
					'string.empty': 'Account type is required',
					'any.only': 'Invalid account type',
				}),
			riderType: Joi.when('type', {
				is: 'Rider',
				then: Joi.string()
					.valid(...validRiderTypes)
					.required()
					.messages({
						'string.base': 'Rider Type must be a string',
						'string.empty': 'Rider Type cannot be empty',
						'any.only': `Rider Type must be one of ${validRiderTypes.join(', ')}`,
						'any.required': 'Rider Type is required for riders',
					}),
				otherwise: Joi.forbidden().strip(),
			}),
			partner: Joi.when('riderType', {
				is: 'Logistic-Partner',
				then: Joi.object({
					companyName: Joi.string().required().label('Company Name').messages({
						'any.required': 'Partner company name is required',
					}),
					ride: Joi.object({
						plateNumber: Joi.string().required().messages({
							'any.required': 'Ride Plate Number is required',
						}),
						description: Joi.string().required().messages({
							'any.required': 'Ride description is required',
						}),
						type: Joi.string()
							.valid(...validRideTypes)
							.required()
							.messages({
								'any.required': 'Ride type is required',
								'any.only': `Ride type must be one of ${validRideTypes.join(', ')}`,
							}),
					}).required(),
				}).required(),

				otherwise: Joi.forbidden().strip(),
			}),
			deliveryAddress: Joi.object({
				address: Joi.string().required(),
				city: Joi.string(),
				nearestLandmark: Joi.string(),
				country: Joi.string(),
				contactName: Joi.string().required(),
				contactPhone: Joi.string()
					.regex(/^(\+?234|0)[\d]{10}$/)
					.required(),
			})
				.when('type', {
					is: 'Customer',
					then: Joi.required(),
					otherwise: Joi.forbidden(),
				})
				.messages({
					'string.empty': 'Address cannot be empty',
				}),
			restaurantName: Joi.string()
				.when('type', { is: 'Vendor', then: Joi.required() })
				.trim()
				.messages({
					'string.empty': 'Restaurant name is required',
				}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateUpdateUserImage(req) {
		const validationSchema = Joi.object({
			profileImage: Joi.string().required().messages({
				'string.empty': 'Profile image cannot be empty',
				'any.required': 'Profile image is required',
			}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}
}
