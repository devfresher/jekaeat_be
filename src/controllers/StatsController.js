import CacheMiddleware from '../middleware/cache.js';
import OrderService from '../services/OrderService.js';

export default class StatsController {
	static async generateRevenueGraph(req, res, next) {
		try {
			const { interval } = req.params;

			const revenueGraphData = await OrderService.generateRevenueGraph(interval);
			CacheMiddleware.cacheResponse(req.originalUrl, revenueGraphData);

			res.status(200).send({
				status: 'success',
				data: revenueGraphData,
			});
		} catch (error) {
			next(error);
		}
	}
}
