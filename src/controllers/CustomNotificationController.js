import customNotificationService from '../services/CustomNotificationService.js';

export class CustomNotificationController {
	schedule = async (req, res, next) => {
		try {
			const { body: data } = req;

			const customNotification = await customNotificationService.create(data);
			return res.status(200).send({
				status: 'success',
				message: 'Custom Notification Scheduled',
				data: customNotification,
			});
		} catch (error) {
			next(error);
		}
	};

	index = async (req, res, next) => {
		try {
			const {
				query: { status },
			} = req;

			const customNotifications = await customNotificationService.getAll(status);
			return res.status(200).send({
				status: 'success',
				message: 'Custom Notifications retrieved',
				data: customNotifications,
			});
		} catch (error) {
			next(error);
		}
	};

	invokeNow = async (req, res, next) => {
		try {
			const {
				params: { jobId },
			} = req;

			const notification = await customNotificationService.invokeNotification(jobId);
			return res.status(200).send({
				status: 'success',
				message: 'Custom Notification invoked',
				data: notification,
			});
		} catch (error) {
			next(error);
		}
	};

	delete = async (req, res, next) => {
		try {
			const {
				params: { jobId },
			} = req;

			await customNotificationService.deleteScheduledNotification(jobId);
			return res.status(204).send();
		} catch (error) {
			next(error);
		}
	};

	reschedule = async (req, res, next) => {
		try {
			const {
				params: { jobId },
				body: rescheduleData,
			} = req;

			const notification = await customNotificationService.reschedule(jobId, rescheduleData);
			return res.status(200).send({
				status: 'success',
				message: 'Custom notification restarted',
				data: notification,
			});
		} catch (error) {
			next(error);
		}
	};

	stop = async (req, res, next) => {
		try {
			const {
				params: { jobId },
			} = req;

			const notification = await customNotificationService.stopScheduledNotification(jobId);
			return res.status(200).send({
				status: 'success',
				message: 'Notification stopped',
				data: notification,
			});
		} catch (error) {
			next(error);
		}
	};
}
