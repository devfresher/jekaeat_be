import { Joi } from '../utils/joi.util.js';

import FeedbackService from '../services/FeedbackService.js';
import { NotFoundError } from '../errors/index.js';

export default class FeedbackController {
	static async sendFeedback(req, res, next) {
		try {
			const {
				user: { _id: userId },
				body: { message },
			} = req;

			await FeedbackService.sendFeedback(userId, message);
			return res.status(200).send({
				status: 'success',
				message: 'Feedback sent',
			});
		} catch (error) {
			next(error);
		}
	}

	static async getAllFeedbacks(req, res, next) {
		try {
			const { query: pageFilter } = req;

			const feedbacks = await FeedbackService.getMany({}, pageFilter);
			return res.status(200).send({
				status: 'success',
				data: feedbacks,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getFeedback(req, res, next) {
		try {
			const {
				params: { feedbackId },
			} = req;

			const feedback = await FeedbackService.getOne({ _id: feedbackId });
			if (!feedback) throw new NotFoundError('Feedback not found');

			return res.status(200).send({
				status: 'success',
				data: feedback,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateNewFeedback(req) {
		const schema = Joi.object({
			message: Joi.string().trim().required().max(300).messages({
				'string.base': 'Message must be a string',
				'string.empty': 'Message cannot be empty',
				'any.required': 'Message is required',
			}),
		});

		return schema.validate(req.body, { abortEarly: false });
	}
}
