import { NotFoundError } from '../errors/index.js';
import PayoutService from '../services/PayoutService.js';
import PaystackService from '../services/PaystackService.js';

export default class PayoutController {
	static async getAllPayouts(req, res, next) {
		try {
			const filterQuery = {};
			if (req.query.status) filterQuery.status = req.query.status;

			const payouts = await PayoutService.getMany(filterQuery, req.query);

			return res.status(200).send({
				message: 'Payouts retrieved',
				status: 'success',
				data: payouts,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getPaystackBalance(req, res, next) {
		try {
			const { data } = await PaystackService.getBalance();

			return res.status(200).send({
				message: 'Balance retrieved',
				status: 'success',
				data: data[0],
			});
		} catch (error) {
			next(error);
		}
	}

	static async verifyBankAccount(req, res, next) {
		try {
			const {
				query: { accountNumber, bankCode },
			} = req;

			const bankAccount = await PaystackService.verifyAccountNumber(accountNumber, bankCode);
			return res.status(200).send({
				status: 'success',
				data: bankAccount,
			});
		} catch (error) {
			next(error);
		}
	}

	static async processPayout(req, res, next) {
		try {
			const { payoutId } = req.params;
			const payout = await PayoutService.getOne({ _id: payoutId });

			if (!payout) throw new NotFoundError('Payout not found');

			await PayoutService.processPayout(payout);

			return res.status(200).send({
				status: 'Success',
				message: 'Payout processed',
			});
		} catch (error) {
			next(error);
		}
	}
}
