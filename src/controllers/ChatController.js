import { Joi } from '../utils/joi.util.js';

import ChatService from '../services/ChatService.js';
import UserService from '../services/UserService.js';
import { BadRequestError, NotFoundError } from '../errors/index.js';

export default class ChatController {
	static async sendMessage(req, res, next) {
		try {
			const {
				user: { _id: senderId },
				body: { recipientId, message, messageType },
			} = req;

			if (senderId === recipientId)
				throw new BadRequestError('You cannot send a message to yourself');

			const recipient = await UserService.getOne({ _id: recipientId });
			if (!recipient) throw new BadRequestError('Invalid recipient ID');

			if (recipient.__t === req.user.userType)
				if (!recipient)
					throw new BadRequestError(`You cannot send message to a ${req.user.userType}`);

			await ChatService.sendMessage(senderId, recipientId, message, messageType);
			return res.status(200).send({
				message: 'Message Sent',
				status: 'success',
			});
		} catch (error) {
			next(error);
		}
	}

	static async getMyChats(req, res, next) {
		try {
			const {
				user: { _id: userId },
			} = req;

			const chats = await ChatService.groupUserChats(userId);
			return res.status(200).send({
				message: 'Messages retrieved',
				status: 'success',
				data: chats,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getConversation(req, res, next) {
		try {
			const {
				user: { _id: userId },
				params: { userId: otherUserId },
			} = req;

			const otherUser = await UserService.getOne({ _id: otherUserId });
			if (!otherUser) throw new NotFoundError('User not found');

			const conversation = await ChatService.getConversations(userId, otherUserId);
			return res.status(200).send({
				message: 'Conversation retrieved',
				status: 'success',
				data: conversation,
			});
		} catch (error) {
			next(error);
		}
	}

	static async readMessage(req, res, next) {
		try {
			const {
				user: { _id: recipientId },
				params: { messageId },
			} = req;

			let message = await ChatService.getOne({ _id: messageId });
			if (!message || recipientId.toString() !== message.recipient._id.toString())
				throw new NotFoundError('Chat message not available');

			message = await ChatService.markAsRead(messageId);
			return res.status(200).send({
				status: 'success',
			});
		} catch (error) {
			next(error);
		}
	}

	static validateSendMessage(req) {
		const messageTypes = {
			Customer: ['order', 'chat', 'media'],
			Vendor: ['orderResponse', 'chat', 'media'],
		};

		const schema = Joi.object({
			message: Joi.string().trim().required().max(1000).messages({
				'string.base': 'Message must be a string',
				'string.empty': 'Message cannot be empty',
				'any.required': 'Message is required',
			}),
			recipientId: Joi.objectId().required().messages({
				'any.required': 'Recipient ID is required',
				'string.pattern.base': 'Invalid Recipient ID format',
				'objectid.isValid': 'Invalid Recipient ID',
			}),
			messageType: Joi.string()
				.valid(...messageTypes[req.user.userType])
				.required()
				.messages({
					'string.base': 'Message Type must be a string',
					'string.empty': 'Message Type cannot be empty',
					'any.only': `Message Type must be one of ${messageTypes[req.user.userType].join(
						', '
					)}`,
					'any.required': 'Message Type is required',
				}),
		});

		return schema.validate(req.body, { abortEarly: false });
	}
}
