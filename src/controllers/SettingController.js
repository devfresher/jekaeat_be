import SettingService from '../services/SettingService.js';
import { Joi } from '../utils/joi.util.js';

export default class SettingController {
	static async index(req, res, next) {
		try {
			const settings = await SettingService.getMany({});

			return res.status(200).send({
				message: 'System settings retrieved',
				status: 'success',
				data: settings,
			});
		} catch (error) {
			next(error);
		}
	}

	static async toggleOrdering(req, res, next) {
		try {
			let orderingStatus = await SettingService.getOne('ordering');
			const setting = await SettingService.upsert('ordering', !orderingStatus);

			return res.status(200).send({
				message: setting.value === true ? 'Ordering enabled' : 'Ordering disabled',
				status: 'success',
				data: setting,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getOrderingStatus(req, res, next) {
		try {
			const orderingStatus = await SettingService.getOne('ordering');

			return res.status(200).send({
				message: 'Ordering status retrieved',
				status: 'success',
				data: orderingStatus,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateToggleOrdering(req) {
		const validationSchema = Joi.object({});

		return validationSchema.validate(req.body, { abortEarly: false });
	}
}
