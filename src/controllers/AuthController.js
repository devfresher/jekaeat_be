import { Joi } from '../utils/joi.util.js';
import { validUserTypes } from '../db/models/User.js';
import AuthService from '../services/AuthService.js';
import UserService from '../services/UserService.js';
import { NotFoundError } from '../errors/index.js';

export default class AuthController {
	static async signIn(req, res, next) {
		try {
			const {
				body: { email, password, type },
			} = req;
			const signInData = await AuthService.signIn(email, password, type);
			return res.status(200).send({
				status: 'success',
				data: signInData,
			});
		} catch (error) {
			next(error);
		}
	}

	static async adminSignIn(req, res, next) {
		try {
			const {
				body: { email, password },
			} = req;

			const signInData = await AuthService.adminSignIn(email, password);
			return res.status(200).send({
				status: 'success',
				data: signInData,
			});
		} catch (error) {
			next(error);
		}
	}

	static async requestPasswordReset(req, res, next) {
		try {
			const {
				body: { email },
			} = req;
			await AuthService.sendOtp(email);
			return res.status(200).send({
				status: 'success',
				message: `OTP sent to ${email}`,
			});
		} catch (error) {
			next(error);
		}
	}

	static async handleOTPValidation(req, res, next) {
		try {
			const {
				body: { email, otp },
			} = req.body;
			const user = await UserService.getOne({ email });
			if (!user) throw new NotFoundError('User with this email does not exist');

			await AuthService.validateOtp(user, otp);
			return res.status(200).send({
				status: 'success',
				message: `OTP validated successfully`,
			});
		} catch (error) {
			next(error);
		}
	}

	static async resetPassword(req, res, next) {
		try {
			const {
				body: { email, otp, newPassword },
			} = req;
			const user = await UserService.getOne({ email });
			if (!user) throw new NotFoundError('User with this email does not exist');

			await AuthService.validateOtp(user, otp);
			await UserService.resetUserPassword(user, newPassword);

			return res.status(200).send({
				status: 'success',
				message: `Password reset completed`,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateAdminSignIn(req) {
		const validationSchema = Joi.object({
			email: Joi.string().email().required().trim().lowercase().messages({
				'string.email': 'Email must be a valid email address',
				'string.empty': 'Email is required',
			}),
			password: Joi.string().required().trim().messages({
				'string.empty': 'Password is required',
			}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateSignIn(req) {
		const validationSchema = Joi.object({
			email: Joi.string().email().required().trim().lowercase().messages({
				'string.email': 'Email must be a valid email address',
				'string.empty': 'Email is required',
			}),
			password: Joi.string().required().trim().messages({
				'string.empty': 'Password is required',
			}),
			type: Joi.string()
				.required()
				.valid(...validUserTypes)
				.trim()
				.messages({
					'string.empty': 'Account type is required',
					'any.only': 'Invalid account type',
				}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateRequestPasswordReset(req) {
		const validationSchema = Joi.object({
			email: Joi.string().email().required().trim().lowercase().messages({
				'string.email': 'Email must be a valid email address',
				'string.empty': 'Email is required',
			}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateOtpRequest(req) {
		const validationSchema = Joi.object({
			email: Joi.string().email().required().trim().lowercase().messages({
				'string.email': 'Email must be a valid email address',
				'string.empty': 'Email is required',
			}),
			otp: Joi.string().min(6).max(6).required().messages({
				'string.max': 'OTP code must be exactly 6 digits',
				'string.min': 'OTP code must be exactly 6 digits',
				'string.empty': 'OTP is required',
			}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}

	static validateChangePassword(req) {
		const validationSchema = Joi.object({
			email: Joi.string().email().required().trim().lowercase().messages({
				'string.email': 'Email must be a valid email address',
				'string.empty': 'Email is required',
			}),
			otp: Joi.string().min(6).max(6).required().messages({
				'string.max': 'OTP code must be exactly 6 digits',
				'string.min': 'OTP code must be exactly 6 digits',
				'string.empty': 'OTP is required',
			}),
			newPassword: Joi.string().required().trim().messages({
				'string.empty': 'New Password is required',
			}),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}
}
