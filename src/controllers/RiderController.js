import { Joi } from '../utils/joi.util.js';
import RiderService from '../services/RiderService.js';
import Utils from '../utils/Utils.js';
import { validRideTypes, validRiderTypes } from '../db/models/Rider.js';
import CacheMiddleware from '../middleware/cache.js';
import { NotFoundError } from '../errors/index.js';

const { daysOfWeek } = Utils;

export default class RiderController {
	static async getAllRiders(req, res, next) {
		try {
			const {
				query: { page, limit },
				originalUrl,
			} = req;

			const pageFilter = { page, limit };
			const riders = await RiderService.getManyRidersWithStats({}, pageFilter);
			CacheMiddleware.cacheResponse(originalUrl, riders);

			return res.status(200).send({
				status: 'success',
				data: riders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getDashboardStats(req, res, next) {
		try {
			const { originalUrl } = req;

			const statistics = await RiderService.getOverallUserStatistics();
			CacheMiddleware.cacheResponse(originalUrl, statistics);

			return res.status(200).send({
				status: 'success',
				data: statistics,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getAvailableRiders(req, res, next) {
		try {
			const {
				query: { page, limit },
			} = req;

			const pageFilter = { page, limit };
			const riders = await RiderService.getAvailableRiders({}, pageFilter);

			return res.status(200).send({
				status: 'success',
				data: riders,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getRider(req, res, next) {
		try {
			const {
				params: { riderId },
			} = req;

			const rider = await RiderService.getOne({ _id: riderId });
			if (!rider) throw new NotFoundError('Rider not found');

			return res.status(200).send({
				status: 'success',
				data: rider,
			});
		} catch (error) {
			next(error);
		}
	}

	static async getRiderWithStats(req, res, next) {
		try {
			const {
				params: { riderId },
				originalUrl,
			} = req;

			let rider = await RiderService.getOne({ _id: riderId });
			if (!rider) throw new NotFoundError('Rider not found');

			rider = await RiderService.getOneRiderWithStats({ _id: riderId });
			CacheMiddleware.cacheResponse(originalUrl, rider);

			return res.status(200).send({
				status: 'success',
				data: rider,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updateProfile(req, res, next) {
		try {
			const {
				user: { _id: riderId },
			} = req;

			let rider = await RiderService.getOne({ _id: riderId });
			if (!rider) throw new NotFoundError('Rider not found');

			rider = await RiderService.updateProfile(rider, req.body);
			return res.status(200).send({
				status: 'success',
				data: rider,
			});
		} catch (error) {
			next(error);
		}
	}

	static async updateRider(req, res, next) {
		try {
			const {
				params: { riderId },
			} = req;

			let rider = await RiderService.getOne({ _id: riderId });
			if (!rider) throw new NotFoundError('Rider not found');

			rider = await RiderService.updateProfile(rider, req.body);

			return res.status(200).send({
				status: 'success',
				data: rider,
			});
		} catch (error) {
			next(error);
		}
	}

	static validateUpdateProfile(req) {
		const validationSchema = Joi.object({
			fullName: Joi.string().trim().messages({
				'string.empty': 'Full name cannot be empty',
			}),
			phoneNumber: Joi.string()
				.regex(/^(\+?234|0)[\d]{10}$/)
				.messages({
					'string.empty': 'Phone Number cannot be empty',
					'string.pattern.base': 'Invalid phone number',
				}),
			riderType: Joi.string()
				.valid(...validRiderTypes)
				.required()
				.messages({
					'string.base': 'Rider Type must be a string',
					'string.empty': 'Rider Type cannot be empty',
					'any.only': `Rider Type must be one of ${validRiderTypes.join(', ')}`,
					'any.required': 'Rider Type is required for riders',
				}),
			partner: Joi.when('riderType', {
				is: 'Logistic-Partner',
				then: Joi.object({
					companyName: Joi.string().label('Company Name'),
					ride: Joi.object({
						plateNumber: Joi.string(),
						description: Joi.string(),
						type: Joi.string()
							.valid(...validRideTypes)
							.required()
							.messages({
								'any.only': `Ride type must be one of ${validRideTypes.join(', ')}`,
							}),
					}),
				}),
				otherwise: Joi.forbidden().strip(),
			}),
			schedule: Joi.array().items(
				Joi.object({
					day: Joi.string()
						.valid(...daysOfWeek)
						.required()
						.messages({
							'any.only': `Day must be one of: ${daysOfWeek}`,
							'any.required': 'Day cannot be empty',
						}),
					openAt: Joi.string()
						.pattern(/^([01]\d|2[0-3]):([0-5]\d)$/)
						.required()
						.messages({
							'string.pattern.base':
								'Opening Time must be a string in the format of hh:mm (e.g., 07:45)',
							'any.required': 'Opening Time cannot be empty',
						}),
					closeAt: Joi.string()
						.pattern(/^([01]\d|2[0-3]):([0-5]\d)$/)
						.required()
						.messages({
							'string.pattern.base':
								'Closing Time must be a string in the format of hh:mm (e.g., 22:00)',
							'any.required': 'Closing Time cannot be empty',
						}),
				})
			),
		});

		return validationSchema.validate(req.body, { abortEarly: false });
	}
}
