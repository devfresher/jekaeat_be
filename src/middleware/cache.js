import winston from 'winston';
import redisClient from '../utils/redis.util.js';
import { SystemError } from '../errors/index.js';

export default class CacheMiddleware {
	static async returnCachedData(req, res, next) {
		const key = `endpoint:${req.originalUrl}`;
		try {
			const cachedData = await redisClient.get(key);
			if (cachedData) {
				return res.status(200).send({
					isCache: true,
					status: 'success',
					data: JSON.parse(cachedData),
				});
			} else {
				next();
			}
		} catch (error) {
			winston.error('Redis error ', error);
			next(error);
		}
	}

	static async cacheResponse(endpoint, data) {
		const key = `endpoint:${endpoint}`;

		try {
			await redisClient.set(key, JSON.stringify(data), 'EX', 600);
		} catch (error) {
			throw new SystemError(500, 'Redis Error', error);
		}
	}
}
