import _ from 'lodash';
import { isValidObjectId } from 'mongoose';
import { BadRequestError } from '../errors/index.js';

export default class ValidationMiddleware {
	static validateRequest = (validator) => {
		return async (req, res, next) => {
			const { error, value } = await validator(req);
			if (error) {
				const errorMessage = error.details[0].message;
				throw new BadRequestError(errorMessage);
			}

			req.body = value;
			next();
		};
	};

	static validateObjectIds = (idNames) => {
		return (req, res, next) => {
			try {
				idNames = Array.isArray(idNames) ? idNames : [idNames];
				const invalidId = _.find(idNames, (idName) => !isValidObjectId(req.params[idName]));
				// if (invalidId) throw new BadRequestError(`Invalid ${invalidId} passed`);

				next();
			} catch (error) {
				next(error);
			}
		};
	};

	static validateQueryObjectIds = (idNames) => {
		return (req, res, next) => {
			try {
				idNames = Array.isArray(idNames) ? idNames : [idNames];
				const invalidId = _.find(idNames, (idName) => !isValidObjectId(req.query[idName]));
				if (invalidId) throw new BadRequestError(`Invalid ${invalidId} passed`);

				next();
			} catch (error) {
				next(error);
			}
		};
	};
}
