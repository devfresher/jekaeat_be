import winston from 'winston';
import { SystemError } from '../errors/index.js';
import { config } from '../utils/config.js';

class SystemMiddleware {
	response = (info, req, res, next) => {
		const isProduction = config.NODE_ENV === 'production';
		const defaultErrorCode = 500;
		const defaultErrorMessage = 'Something unexpected went wrong';

		const errorCode = info.code || defaultErrorCode;
		const errorMessage =
			errorCode == defaultErrorCode && isProduction
				? defaultErrorMessage
				: info.message || defaultErrorMessage;

		if (res.headersSent) {
			next(info);
		}

		if (info instanceof SystemError) {
			return res.status(errorCode).send({
				status: 'error',
				error: {
					code: errorCode,
					message: errorMessage,
					...(info.errors && isProduction && { error: info.errors }),
				},
			});
		}

		let code, message;
		switch (info.status) {
			case 'success':
				code = this.isValidStatusCode(info.code) ? info.code : 200;

				return res.status(code).json({
					status: info.status,
					data: info.data,
					message: info.message,
				});

			case 'error':
				code = this.isValidStatusCode(info.code) ? info.code : errorCode;
				message = info.message || errorMessage;

				if (code === 500) winston.error(message, info);

				return res.status(code).json({
					status: info.status,
					error: { code, message },
				});

			default:
				code = errorCode;
				message = errorMessage;

				winston.error(message, info);

				return res.status(code).json({
					status: 'error',
					error: { code, message },
				});
		}
	};

	isValidStatusCode = (code) => {
		return code >= 100 && code <= 599;
	};
}

export default new SystemMiddleware();
