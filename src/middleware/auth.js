import jwt from 'jsonwebtoken';

import { config } from '../utils/config.js';

import UserService from '../services/UserService.js';
import AdminService from '../services/AdminService.js';
import { ForbiddenError, UnauthorizedError } from '../errors/index.js';

export default class AuthMiddleware {
	static async authenticateToken(req, res, next) {
		try {
			const authHeader = req.headers['authorization'];
			const token = authHeader && authHeader.replace(/^Bearer\s+/, '');
			if (!token) throw new UnauthorizedError('Access denied. No auth token provided');

			const decodedToken = jwt.verify(token, config.JWT_PRIVATE_KEY);
			const user =
				decodedToken.userType == 'Admin'
					? await AdminService.getOne({ _id: decodedToken._id })
					: await UserService.getOne({ _id: decodedToken._id });

			if (!user) throw new UnauthorizedError('Invalid auth token');

			if (!user.isActive)
				throw new ForbiddenError(
					'Your account has been deactivated. Please contact support for assistance.'
				);

			req.user = decodedToken;
			return next();
		} catch (error) {
			if (error.name === 'TokenExpiredError')
				error = new UnauthorizedError('Auth token expired');

			next(error);
		}
	}

	static authenticateUserType(roles) {
		return async (req, res, next) => {
			try {
				await this.authenticateToken(req, res, (error) => {
					if (error) throw error;

					const {
						user: { userType },
					} = req;

					roles = Array.isArray(roles) ? roles : [roles];
					if (!roles.includes(userType))
						throw new ForbiddenError('You are forbidden to take this action');

					next();
				});
			} catch (error) {
				next(error);
			}
		};
	}
}
