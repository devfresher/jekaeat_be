import multer from 'multer';
import path from 'path';

import Utils from '../utils/Utils.js';
import { BadRequestError } from '../errors/index.js';

export default class FileUploadMiddleware {
	static #supportedFormats = {
		image: ['.jpeg', '.jpg', '.png'],
		csv: ['.csv'],
	};

	static #maxFileSize = 60 * 1024 * 1024;

	static #createMulter(fieldName, formats, destination) {
		return multer({
			storage: multer.diskStorage({
				destination: (req, file, cb) => {
					cb(null, destination);
				},
				filename: (req, file, cb) => {
					cb(null, file.originalname);
				},
			}),
			fileFilter: (req, file, cb) => {
				const ext = path.extname(file.originalname);
				if (!formats.includes(ext)) {
					req.fileValidationError = `Unsupported file format. Expecting ${formats.join(
						', '
					)} file(s) only`;
					cb(null, false);
				} else {
					cb(null, true);
				}
			},
			limits: { fileSize: this.#maxFileSize, files: 1 },
		}).single(fieldName);
	}

	static #uploadFile(fieldName, format, destination = 'tempUploads') {
		const absDestination = path.resolve(process.cwd(), destination);
		Utils.createDestination(absDestination);

		const upload = this.#createMulter(fieldName, this.#supportedFormats[format], destination);

		return (req, res, next) => {
			try {
				upload(req, res, (err) => {
					if (err) throw err;

					if (err instanceof multer.MulterError) {
						throw new BadRequestError(err.message);
					} else if (req.fileValidationError) {
						throw new BadRequestError(req.fileValidationError);
					}

					next();
				});
			} catch (error) {
				next(error);
			}
		};
	}

	static uploadSingleImage = (fieldName, destination) =>
		FileUploadMiddleware.#uploadFile(fieldName, 'image', destination);

	static uploadBatchCsv = (fieldName, destination) =>
		FileUploadMiddleware.#uploadFile(fieldName, 'csv', destination);
}
