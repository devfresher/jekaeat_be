import { mongoose } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import aggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { now } from '../../utils/day.util.js';

export const validUserTypes = ['Customer', 'Vendor', 'Rider'];
export const periodicPayoutSettlement = ['Immediate', 'Daily', 'Weekly', 'Monthly'];

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	phoneNumber: String,
	passwordReset: {
		type: {
			token: String,
			expiry: Date,
			_id: false,
		},
	},
	profileImage: {
		type: {
			url: String,
			imageId: String,
			_id: false,
		},
	},
	createdAt: { type: Date, default: () => now().toDate() },
});

userSchema.plugin(paginate);
userSchema.plugin(aggregatePaginate);
const User = mongoose.model('User', userSchema);
export default User;
