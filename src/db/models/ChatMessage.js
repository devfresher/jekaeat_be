import { mongoose, SchemaTypes } from 'mongoose';
import { now } from '../../utils/day.util.js';

export const messageTypes = ['order', 'orderResponse', 'chat', 'media'];
const chatMessageSchema = new mongoose.Schema({
	sender: {
		type: SchemaTypes.ObjectId,
		ref: 'User',
		required: true,
	},
	recipient: {
		type: SchemaTypes.ObjectId,
		ref: 'User',
		required: true,
	},
	message: {
		type: String,
		required: true,
	},
	messageType: {
		type: String,
		enum: messageTypes,
		default: 'chat',
		required: true,
	},
	readAt: Date,
	createdAt: {
		type: Date,
		default: () => now().todate(),
	},
});

const ChatMessage = mongoose.model('ChatMessage', chatMessageSchema);
export default ChatMessage;
