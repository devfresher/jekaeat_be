import { mongoose } from "mongoose"

import User, { periodicPayoutSettlement } from "./User.js"

const vendorSchema = new mongoose.Schema({
	restaurantName: { type: String, required: true },
	restaurantLocation: {
		type: {
			address: { type: String },
			city: { type: String },
			landmark: {
				type: {
					location: { type: String },
					zone: { type: String },
					_id: false,
				},
			},
			_id: false,
		},
	},
	schedule: {
		type: [{ day: String, openAt: String, closeAt: String, _id: false }],
	},
	description: { type: String },
	payoutSettlement: {
		account: {
			accountName: String,
			accountNumber: String,
			bank: String,
			subAccountCode: String,
			_id: false,
		},
		allowPeriodicPayout: {
			type: Boolean,
			default: false,
		},
		period: {
			type: String,
			enum: periodicPayoutSettlement,
			default: "Immediate",
		},
	},
	featured: {
		type: Boolean,
		default: false,
	},
	shopClosed: {
		type: Boolean,
		default: false,
	},
})

const Vendor = User.discriminator("Vendor", vendorSchema)
export default Vendor
