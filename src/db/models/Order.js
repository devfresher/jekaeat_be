import { mongoose, SchemaTypes } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { now } from '../../utils/day.util.js';

export const paymentMethods = ['Online', 'Delivery'];
export const paymentStatus = ['Unpaid', 'Pending', 'Paid', 'Failed'];
export const orderStatus = [
	'Pending',
	'Ready',
	'Accepted',
	'Dispatching',
	'Delivered',
	'Cancelled',
];

const statusUpdateSchema = new mongoose.Schema({
	_id: false,
	status: {
		type: String,
		required: true,
	},
	updatedAt: {
		type: Date,
		required: true,
		default: () => now().toDate(),
	},
	updatedBy: {
		type: SchemaTypes.ObjectId,
		ref: 'User',
		required: true,
	},
});

const orderSchema = new mongoose.Schema({
	customer: { type: SchemaTypes.ObjectId, ref: 'Customer', required: true },
	vendor: { type: SchemaTypes.ObjectId, ref: 'Vendor', required: true },
	rider: { type: SchemaTypes.ObjectId, ref: 'Rider' },
	packages: [
		{
			name: { type: String },
			meals: [
				{
					meal: { type: SchemaTypes.ObjectId, ref: 'Meal' },
					quantity: { type: Number, required: true },
					price: { type: Number, required: true },
					_id: false,
				},
			],
			total: { type: Number, required: true },
			_id: false,
		},
	],
	total: { type: Number, required: true },
	serviceFee: { type: Number, required: true, default: 0 },
	status: { type: String, enum: orderStatus, default: 'Pending' },
	token: { type: String, unique: true },
	payment: {
		method: { type: String, enum: paymentMethods, default: 'Online' },
		status: { type: String, enum: paymentStatus, default: 'Unpaid' },
		reference: { type: String },
		_id: false,
	},
	deliveryInfo: {
		fee: { type: Number, default: 0 },
		ridersFee: { type: Number, default: 0 },
		address: { type: String, required: true },
		nearestLandmark: { type: String },
		deliveryInstructions: { type: String },
		contactName: { type: String, required: true },
		contactPhone: { type: String, required: true },
		_id: false,
	},
	statusUpdates: [statusUpdateSchema],
	createdAt: { type: Date, default: () => now().toDate() },
});

orderSchema.plugin(paginate);
const Order = mongoose.model('Order', orderSchema);
export default Order;
