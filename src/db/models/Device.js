import { SchemaTypes, mongoose } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { now } from '../../utils/day.util.js';

const deviceSchema = new mongoose.Schema({
	user: { type: SchemaTypes.ObjectId, ref: 'User' },
	device_token: { type: String, required: true },
	device_type: { type: String, required: true },
	createdAt: { type: Date, default: () => now().toDate() },
});

deviceSchema.plugin(paginate);
const Device = mongoose.model('Device', deviceSchema);
export default Device;
