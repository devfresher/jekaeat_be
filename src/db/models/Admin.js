import { mongoose } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { now } from '../../utils/day.util.js';

const adminSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	isSuperAdmin: {
		type: Boolean,
		default: false,
	},
	resetPasswordToken: String,
	resetTokenExpiry: Date,
	profileImage: {
		type: {
			url: String,
			imageId: String,
			_id: false,
		},
	},
	createdAt: { type: Date, default: () => now().toDate() },
});

adminSchema.plugin(paginate);
const Admin = mongoose.model('Admin', adminSchema);
export default Admin;
