import { SchemaTypes, mongoose } from 'mongoose';
import { now } from '../../utils/day.util.js';
const settingsSchema = new mongoose.Schema({
	key: {
		type: String,
		required: true,
		unique: true,
	},
	value: {
		type: SchemaTypes.Mixed,
		required: true,
	},
	createdAt: { type: Date, default: () => now().toDate() },
	updatedAt: { type: Date },
});

const Setting = mongoose.model('Setting', settingsSchema);
export default Setting;
