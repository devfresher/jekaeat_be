import { mongoose } from 'mongoose';
import { now } from '../../utils/day.util.js';
import { validUserTypes } from './User.js';

export const customNotificationStatuses = ['active', 'inactive'];
const customNotificationSchema = new mongoose.Schema({
	name: { type: String, required: true },
	label: { type: String, required: true },
	time: { type: String, required: true },
	days: {
		type: [Number],
	},
	userType: { type: String, enum: validUserTypes },
	status: { type: String, enum: customNotificationStatuses, default: 'active' },
	notification: {
		title: { type: String },
		body: { type: String },
		_id: false,
	},
	createdAt: { type: Date, default: () => now().toDate() },
});

const CustomNotification = mongoose.model('custom_notifications', customNotificationSchema);
export default CustomNotification;
