import { mongoose } from 'mongoose';
import User, { periodicPayoutSettlement } from './User.js';

export const validRiderTypes = ['Freelance', 'Full-Time', 'Logistic-Partner'];
export const validRideTypes = ['Bike', 'Bicycle', 'Car'];

const riderSchema = new mongoose.Schema({
	phoneNumber: { type: String, required: true },
	schedule: {
		type: [{ day: String, openAt: String, closeAt: String, _id: false }],
	},
	type: { type: String, enum: validRiderTypes, default: 'Full-Time' },
	partner: {
		_id: false,
		companyName: String,
		ride: {
			_id: false,
			plateNumber: String,
			description: String,
			type: { type: String, enum: validRideTypes },
		},
	},
	payoutSettlement: {
		account: {
			accountName: String,
			accountNumber: String,
			bank: String,
			subAccountCode: String,
			_id: false,
		},
		allowPeriodicPayout: {
			type: Boolean,
			default: true,
		},
		period: {
			type: String,
			enum: periodicPayoutSettlement,
			default: 'Daily',
		},
	},
});

const Rider = User.discriminator('Rider', riderSchema);
export default Rider;
