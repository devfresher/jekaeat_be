import { mongoose, SchemaTypes } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { periodicPayoutSettlement } from './User.js';
import { now } from '../../utils/day.util.js';

const payoutStatus = ['Successful', 'Failed', 'Reversed', 'Pending', 'Not sent'];
const payoutSchema = new mongoose.Schema({
	user: {
		type: SchemaTypes.ObjectId,
		ref: 'User',
		required: true,
	},
	order: {
		type: SchemaTypes.ObjectId,
		ref: 'Order',
	},
	reference: {
		type: String,
	},
	status: {
		type: String,
		enum: payoutStatus,
		default: 'Pending',
	},
	amount: { type: Number },
	processPeriod: { type: String, enum: periodicPayoutSettlement },
	createdAt: { type: Date, default: () => now().toDate() },
	processedAt: { type: Date },
});

payoutSchema.plugin(paginate);
const Payout = mongoose.model('Payout', payoutSchema);
export default Payout;
