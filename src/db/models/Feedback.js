import { mongoose, SchemaTypes } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { now } from '../../utils/day.util.js';

const feedbackSchema = new mongoose.Schema({
	user: {
		type: SchemaTypes.ObjectId,
		ref: 'User',
		required: true,
	},
	message: {
		type: String,
		required: true,
	},
	readAt: Date,
	createdAt: {
		type: Date,
		default: () => now().toDate(),
	},
});

feedbackSchema.plugin(paginate);
const Feedback = mongoose.model('Feedback', feedbackSchema);
export default Feedback;
