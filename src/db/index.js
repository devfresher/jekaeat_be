import mongoose from 'mongoose';
import { config } from '../utils/config.js';

export const connectDB = async () => {
	mongoose.set('strictQuery', false);
	mongoose.connect(config.DB_URL);
	return mongoose.connection;
};
