import axios from 'axios';
import { config } from '../utils/config.js';

const apiClient = axios.create({
	baseURL: `https://graph.facebook.com/${config.WHATSAPP_API_VERSION}/${config.WHATSAPP_PHONE_NUMBER_ID}`,
	headers: {
		Authorization: `Bearer ${config.WHATSAPP_ACCESS_TOKEN}`,
		'Content-Type': 'application/json',
	},
});

class WhatsApp_ApiService {
	async sendMessage(data) {
		try {
			const response = await apiClient.post('messages', data);
			return response.data;
		} catch (error) {
			winston.info('Error sending WhatsApp notification', error);
		}
	}

	async getMessageInput(recipient, messageType) {
		return JSON.stringify({
			messaging_product: 'whatsapp',
			to: recipient,
			type: 'template',
			template: {
				name: messageType,
				language: { code: 'en_US' },
			},
		});
	}
}

export default new WhatsApp_ApiService();
