export const messages = {
	"vendor:order_available_for_process": {
		title: `A new order is available`,
		body: `Log on to your app to view the order details and process the order as soon as possible to ensure timely delivery.`,
	},
	"vendor:order_rejected_by_rider": {
		title: `Order rejected`,
		body: `A rider rejected an order for delivery A new request for rider has been sent immediately.`,
	},
	"vendor:order_delivered": {
		title: `Order delivered`,
		body: `Order was delivered successfully to your customer.`,
	},

	"customer:payment_confirmed_success": {
		title: `Payment confirmed`,
		body: `Your payment has been received. Your order is currently being processed and will be delivered to you shortly.`,
	},
	"customer:payment_confirmed_failed": {
		title: `Payment failed`,
		body: `Your payment was not confirmed. Contact the support for further assistance.`,
	},
	"customer:order_ready_for_delivery": {
		title: `Order ready for delivery`,
		body: `Your order is fully processed and ready for delivery`,
	},
	"customer:order_accepted_for_dispatch": {
		title: `Get set! Your hunger is set to be quenched`,
		body: `Your order has been scheduled for a pickup by a dispatch rider. Get set you'll be contacted shortly.`,
	},
	"customer:order_delivered": {
		title: `Order delivered!`,
		body: `Hey your order was delivered. Thanks for the patronage, hope to see you soon.`,
	},
	"customer:order_dispatching": {
		title: `Order on its way`,
		body: `Hey chief, your order is on its way to you. Get ready to devour the special delicacy`,
	},

	"rider:order_available_for_dispatch": {
		title: `A new order is available for delivery`,
		body: `Log on to your app to view the order details and accept the order as soon as possible to ensure timely delivery.`,
	},
}
