import axios from 'axios';
import { config } from '../utils/config.js';
import _ from 'lodash';
import { SystemError } from '../errors/index.js';
import winston from 'winston';

const apiClient = axios.create({
	baseURL: `https://fcm.googleapis.com/fcm`,
	headers: {
		Authorization: `key=${config.FIREBASE_KEY}`,
		'Content-Type': 'application/json',
	},
});

class PushNotification {
	async send(device, notification) {
		const requestData = {
			notification: {
				...notification,
				image: config.APP_LOGO,
			},
		};
		if (_.isArray(device)) requestData.registration_ids = device;
		else requestData.to = device;

		try {
			const response = await apiClient.post('send', requestData);
			return response.data;
		} catch (error) {
			winston.error('Error sending push notification: ', error);
		}
	}
}

export default new PushNotification();
