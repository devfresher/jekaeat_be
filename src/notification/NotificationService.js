import DeviceService from '../services/DeviceService.js';
import pushNotification from './FireBaseService.js';
import whatsApp from './WhatsAppService.js';
import { messages } from './template.js';
import winston from 'winston';

export default class Notification {
	static async notify(data, custom = false) {
		try {
			const { user } = data;
			const devices = await DeviceService.getMany({ user: user._id });

			// if (user.id.toString() !== '6479cd3aac8f902a5a66e6e8') return;
			// if (user.id.toString() !== '64d620efc8332fce346d808b') return;
			if (devices.length === 0) return;
			winston.info(`Push Notification Sent to ${user._id} (${user.fullName})`);
			const deviceTokens = devices.map((device) => device.device_token);

			if (custom) {
				const { title, body } = data;
				await this.#handleCustomNotification(deviceTokens, title, body);
			} else {
				const { messageType } = data;
				await this.#handleOrderNotification(deviceTokens, messageType);
			}
		} catch (error) {
			winston.error('Error sending notification: ', error);
		}
	}

	static #handleOrderNotification = async (deviceTokens, messageType) => {
		if (deviceTokens.length === 0) {
			const data = await whatsApp.getMessageInput(user.phoneNumber, messageType);
			// whatsApp.sendMessage(data);
		} else {
			const message = messages[messageType];
			await pushNotification.send(deviceTokens, message);
		}
	};

	static #handleCustomNotification = async (deviceTokens, title, body) => {
		const message = { title, body };
		await pushNotification.send(deviceTokens, message);
	};
}
