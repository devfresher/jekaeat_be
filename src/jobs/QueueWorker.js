import { Worker } from 'bullmq';
import redisClient from '../utils/redis.util.js';
import winston from 'winston';

export class QueueWorker {
	constructor(queueName, jobHandler) {
		this.queueName = queueName;
		this.worker = new Worker(
			queueName,
			async (job) => {
				await jobHandler(job.data);
			},
			{
				connection: redisClient,
			}
		);
	}

	start() {
		this.worker.on('completed', (job) => {
			winston.info(`Job ${job.id} completed`);
		});

		this.worker.on('failed', (job, err) => {
			winston.info(`Job ${job.id} failed: ${err}`);
		});

		winston.info(`Worker for queue '${this.queueName}' started`);
	}
}
