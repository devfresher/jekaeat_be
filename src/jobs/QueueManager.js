import { Queue } from 'bullmq';
import redisClient from '../utils/redis.util.js';
import {
	PROCESS_PAYOUT_QUEUE,
	PUSH_NOTIFICATION_QUEUE,
	SEND_EMAIL_QUEUE,
} from '../utils/constants.util.js';

export default class QueueManager {
	constructor(queueName) {
		this.queueName = queueName;
		this.queue = new Queue(queueName, {
			connection: redisClient,
		});
	}

	async addJob(jobData) {
		// const job = await this.queue.add(this.queueName, jobData);
		// return job;
	}
}

export const emailQueue = new QueueManager(SEND_EMAIL_QUEUE);
export const pushNotificationQueue = new QueueManager(PUSH_NOTIFICATION_QUEUE);
export const processPayoutQueue = new QueueManager(PROCESS_PAYOUT_QUEUE);
