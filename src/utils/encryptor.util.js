import { config } from './config.js';
import crypto, { createCipheriv, createDecipheriv } from 'crypto';

class Encryptor {
	constructor(algorithm, secretKey, iv) {
		this.algorithm = algorithm;
		this.secretKey = secretKey;
		this.iv = crypto.randomBytes(16);
	}

	encrypt = (token) => {
		const cipher = createCipheriv(this.algorithm, Buffer.from(this.secretKey, 'hex'), this.iv);
		let encrypted = cipher.update(token);

		encrypted = Buffer.concat([encrypted, cipher.final()]);

		return { iv: this.iv.toString('hex'), encryptedData: encrypted.toString('hex') };
	};

	decrypt = (data) => {
		this.iv = Buffer.from(data.iv, 'hex');

		const encryptedToken = Buffer.from(data.token, 'hex');
		const decipher = createDecipheriv(
			this.algorithm,
			Buffer.from(this.secretKey, 'hex'),
			this.iv
		);

		let decrypted = decipher.update(encryptedToken);

		decrypted = Buffer.concat([decrypted, decipher.final()]);
		return decrypted.toString();
	};
}

export default new Encryptor(config.ENCRYPTION_ALGO, config.ENCRYPTION_KEY);
