export const SEND_EMAIL_QUEUE = "sendEmail"
export const PROCESS_PAYOUT_QUEUE = "processPayoutQueue"
export const PUSH_NOTIFICATION_QUEUE = "pushNotificationQueue"
