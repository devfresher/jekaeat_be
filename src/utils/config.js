import dotenv from 'dotenv';
const env = !process.env.NODE_ENV ? 'development' : process.env.NODE_ENV;
dotenv.config({ path: `.env.${env}` });

export const config = {
	APP_NAME: `Jekaeat ${env.toUpperCase()}`,
	APP_LOGO:
		'https://res.cloudinary.com/ds1ryba2h/image/upload/v1695051220/jekaeat/others/jekaeat_delivers_white_snm7qk.png',
	HOST: process.env.HOST,
	PORT: parseInt(process.env.PORT) || 3000,
	NODE_ENV: process.env.NODE_ENV,

	REDIS_HOST: process.env.REDIS_HOST || 'localhost',
	REDIS_PORT: process.env.REDIS_PORT || 6379,
	REDIS_URL: process.env.REDIS_URL,

	DB_URL: process.env.DB_URL,
	ADMIN_EMAIL: process.env.ADMIN_EMAIL,
	CUSTOMER_SUPPORT_EMAIL: process.env.CUSTOMER_SUPPORT_EMAIL,

	JWT_PRIVATE_KEY: process.env.JWT_PRIVATE_KEY,

	PAYSTACK_SECRET_KEY: process.env.PAYSTACK_SECRET_KEY,
	PAYSTACK_PUBLIC_KEY: process.env.PAYSTACK_PUBLIC_KEY,

	CLOUDINARY_NAME: process.env.CLOUDINARY_NAME,
	CLOUDINARY_KEY: process.env.CLOUDINARY_KEY,
	CLOUDINARY_SECRET: process.env.CLOUDINARY_SECRET,

	WHATSAPP_APP_ID: process.env.WHATSAPP_APP_ID,
	WHATSAPP_APP_SECRET: process.env.WHATSAPP_APP_SECRET,
	WHATSAPP_API_VERSION: process.env.WHATSAPP_API_VERSION,
	WHATSAPP_PHONE_NUMBER_ID: process.env.WHATSAPP_PHONE_NUMBER_ID,
	WHATSAPP_ACCESS_TOKEN: process.env.WHATSAPP_ACCESS_TOKEN,

	FIREBASE_KEY: process.env.FIREBASE_KEY,

	SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,

	GMAIL_OAUTH: {
		client_id: process.env.GOOGLE_AUTH_CLIENT_ID,
		client_secret: process.env.GOOGLE_AUTH_CLIENT_SECRET,
		redirect_url: `${
			env !== 'development'
				? 'https://jekaeat-main.onrender.com'
				: `http://localhost:${process.env.PORT}`
		}/api/admin/dev/auth/google/callback`,
	},

	ENCRYPTION_ALGO: process.env.ENCRYPTION_ALGO,
	ENCRYPTION_KEY: process.env.ENCRYPTION_KEY,
	ENCRYPTION_IV: process.env.ENCRYPTION_IV,
};
