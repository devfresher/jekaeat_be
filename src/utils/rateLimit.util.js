import rateLimit from 'express-rate-limit';
import RedisStore from 'rate-limit-redis';

import redisClient from './redis.util.js';

class RateLimit {
	limitRequestHandler = () => {
		return rateLimit({
			windowMs: 1 * 60 * 1000,
			max: 50,
			standardHeaders: true,
			legacyHeaders: false,
			message: {
				status: 'error',
				error: {
					code: 409,
					message: 'You are hitting me too fast',
				},
			},
			store: new RedisStore({
				sendCommand: (...args) => redisClient.call(...args),
			}),
		});
	};
}

export default new RateLimit().limitRequestHandler;
