import { Redis } from 'ioredis';
import { config } from './config.js';

class RedisClient {
	constructor(host, port, url) {
		this.host = host;
		this.port = port;
		this.url = url;
		this.client = this.#getClient();
	}

	#getClient = () => {
		return new Redis(
			this.url ? this.url : { host: this.host, port: this.port, maxRetriesPerRequest: null }
		);
	};
}

export default new RedisClient(config.REDIS_HOST, config.REDIS_PORT, config.REDIS_URL).client;
