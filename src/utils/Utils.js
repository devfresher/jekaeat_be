import fs from 'fs';
import crypto from 'crypto';
import dayjs, { now } from './day.util.js';
import { BadRequestError } from '../errors/index.js';

export default class Utils {
	static daysOfWeek = [
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
	];

	static getLabel(name) {
		const label = name.replace(/([^\w ]|_)/g, '');
		return `${label.replace(/\s+/g, '-').toLowerCase()}`;
	}

	static createDestination(absDestination) {
		if (!fs.existsSync(absDestination)) {
			fs.mkdirSync(absDestination);
		}
	}

	static isValidSignature(signature, body, secretKey) {
		const hash = crypto
			.createHmac('sha512', secretKey)
			.update(JSON.stringify(body))
			.digest('hex');

		return hash === signature;
	}

	static generateReference(prefix) {
		const randomBytes = crypto.randomBytes(8);
		const reference = prefix
			? `${prefix}${randomBytes.toString('hex')}`
			: randomBytes.toString('hex');

		return reference;
	}

	// Calculate distance between two points
	static getDistance(point1, point2) {
		const [lon1, lat1] = point1;
		const [lon2, lat2] = point2;

		const R = 6371e3; // radius of the Earth in meters
		const φ1 = this.#toRadians(lat1);
		const φ2 = this.#toRadians(lat2);
		const Δφ = this.#toRadians(lat2 - lat1);
		const Δλ = this.#toRadians(lon2 - lon1);

		const a =
			Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
			Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		const d = R * c; // distance in meters

		return d;
	}

	static #toRadians(degrees) {
		return degrees * (Math.PI / 180);
	}

	static getDateRange(timeFrame = '', customDate = '') {
		if (customDate) {
			const { startDateStr, endDateStr } = customDate;
			const startDate = dayjs(startDateStr, 'YYYY-MM-DD', true);
			const endDate = dayjs(endDateStr, 'YYYY-MM-DD', true);

			if (!startDate.isValid() || !endDate.isValid())
				throw new BadRequestError(
					'Invalid start or end date format. Please use the format YYYY-MM-DD.'
				);

			if (endDate.isBefore(startDate))
				throw new BadRequestError('End date must be after start date.');

			return {
				$gte: startDate.startOf('day').toDate(),
				$lte: endDate.endOf('day').toDate(),
			};
		}

		switch (timeFrame) {
			case 'weekly':
				return {
					$gte: now().startOf('week').toDate(),
					$lte: now().endOf('week').toDate(),
				};
			case 'monthly':
				return {
					$gte: now().startOf('month').toDate(),
					$lte: now().endOf('month').toDate(),
				};
			case 'daily':
				return {
					$gte: now().startOf('day').toDate(),
					$lte: now().endOf('day').toDate(),
				};
			case 'yearly':
				return {
					$gte: now().startOf('year').toDate(),
					$lte: now().endOf('year').toDate(),
				};
			default:
				return {
					$lte: now().toDate(),
				};
		}
	}

	static shuffleArray(array) {
		for (let i = array.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			[array[i], array[j]] = [array[j], array[i]];
		}
		return array;
	}
}
